
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity top is
port (
         clk_25mhz   : in std_logic;
         btn         : in std_logic_vector(6 downto 0);
         led         : out std_logic_vector(7 downto 0);
         gp          : out std_logic_vector(24 downto 0);
         gn          : out std_logic_vector(24 downto 0);
         wifi_gpio0  : out std_logic;

         --sdram interface
        sdram_csn : out std_logic;
        sdram_clk : out std_logic;
        sdram_cke : out std_logic;
        sdram_rasn : out std_logic;
        sdram_casn : out std_logic;
        sdram_wen : out std_logic;
        sdram_a : out std_logic_vector(12 downto 0);
        sdram_ba : out std_logic_vector(1 downto 0);
        sdram_dqm : out std_logic_vector(1 downto 0);
        sdram_d : inout std_logic_vector(15 downto 0);
     );
end top;

architecture Behavioral of top is
    signal clocks : std_logic_vector(3 downto 0);
    signal clk_i, clk_i90, clk_120mhz : std_logic;
    signal count : unsigned(28 downto 0);
    signal s_led : std_logic_vector(7 downto 0);
begin
    clk_i <= clk_25mhz;
    clk_120mhz <= clocks(2);
    wifi_gpio0 <= '1';
    sdram_cke <= '1';

    clkgen_inst: entity work.ecp5pll
    generic map
    (
        in_Hz => natural( 25.0e6),
        out0_Hz => natural( 10.0e6),                  out0_tol_hz => 0,
        out1_Hz => natural( 10.0e6), out1_deg =>  90, out1_tol_hz => 0,
        out2_Hz => natural( 120.0e6)
    )
    port map
    (
        clk_i => clk_25mhz,
        clk_o => clocks
    );

    ice : entity work.ice_soc 
    port map
    (
        clk => clocks(0),
        clk90 => clocks(1),
        reset => btn(1),
        led => s_led,

        sdram_clk120 => clocks(2),
        sdram_csn => sdram_csn,
        sdram_rasn => sdram_rasn,
        sdram_casn => sdram_casn,
        sdram_wen => sdram_wen,
        sdram_a => sdram_a,
        sdram_ba => sdram_ba,
        sdram_dqm => sdram_dqm,
        sdram_d => sdram_d
    );

    led <= s_led;

end Behavioral;
