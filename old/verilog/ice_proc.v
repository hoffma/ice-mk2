module ice_proc(
    input clk,
    input clk90,
    input reset,
    output [11:0] instr_addr,
    input [15:0] instruction,
    output reg [15:0] mem_addr,
    input [15:0] mem_in,
    output reg [15:0] mem_out,
    output reg mem_rd,
    output reg mem_wr
);

localparam word_width = 16;
localparam data_width = 16;
localparam addr_width = 12;

/* jump instructions */
localparam JAL = 4'b0000;
localparam JALR = 4'b0001;
localparam BNE = 4'b0010;
localparam BEQ = 4'b0011;
/* alu instructions */
localparam ADD = 4'b0100;
localparam SUB = 4'b0101;
localparam AND = 4'b0110;
localparam OR = 4'b0111;
localparam XOR = 4'b1000;
localparam LSL = 4'b1001;
localparam LSR = 4'b1010;
localparam SLT = 4'b1011;
/* memory instructions */
localparam LD = 4'b1100;
localparam LDU = 4'b1101;
localparam STM = 4'b1110;
localparam LDM = 4'b1111;

wire i_clk, i_clk90;
assign i_clk = clk;
assign i_clk90 = clk90;
wire s_reset;
assign s_reset = reset;

/* verilator lint_off UNUSED */
reg [7:0] status_reg;

wire [3:0] rf_rp0_addr;
wire [3:0] rf_rp1_addr;
reg [3:0] rf_wp_addr;
wire [15:0] rf_rp0_data;
wire [15:0] rf_rp1_data;
reg [15:0] rf_wp_data;
reg rf_wp_en;

wire [15:0] alu_y;
wire [15:0] mem_y;

reg pc_load;
reg pc_load_inr; /* immediate, not register */

wire [7:0] imm;
wire [11:0] jump_addr;

wire [11:0] pc_pc;
wire [3:0] inst_opc;

assign rf_rp0_addr = instruction[11:8];
assign rf_rp1_addr = instruction[7:4];
assign mem_y = mem_in;
assign imm = instruction[7:0];
assign jump_addr = instruction[11:0];
assign inst_opc = instruction[15:12];

assign instr_addr = pc_pc;

ice_pc inst_pc(
    .clk(i_clk),
    .reset(s_reset),
    .load(pc_load),
    .jmp_addr(jump_addr),
    .pc(pc_pc)
);

ice_rf inst_rf(
    .clk(i_clk90),
    .reset(s_reset),
    .rp0_addr(rf_rp0_addr),
    .rp1_addr(rf_rp1_addr),
    .wp_addr(rf_wp_addr),
    .rp0_data(rf_rp0_data),
    .rp1_data(rf_rp1_data),
    .wp_data(rf_wp_data),
    .wp_en(rf_wp_en)
);

ice_alu inst_alu(
    .a(rf_rp0_data),
    .b(rf_rp1_data),
    .opc(inst_opc),
    .y(alu_y),
    .c(status_reg[0])
);

/* do instruction decoding */
always @(inst_opc)
begin
    pc_load = 0;
    pc_load_inr = 1;
    rf_wp_en = 0;
    rf_wp_data = alu_y;
    rf_wp_addr = instruction[3:0];
    mem_rd = 0;
    mem_wr = 0;
    mem_addr = 16'b0;
    mem_out = 16'b0;
    case(inst_opc)
        JAL: begin
            rf_wp_data = {4'b0, pc_pc};
            rf_wp_addr = 4'b1111;
            rf_wp_en = 1;
            pc_load = 1;
            pc_load_inr = 1;
        end
        JALR: begin
            /* maybe later lmao
            rf_wp_data = {4'b0, imm};
            rf_wp_en = 1;
            pc_load = 1;
            pc_load_inr = 0;
            */
        end

        BNE: begin
            if(rf_rp0_data != 0) begin
                pc_load = 1;
                pc_load_inr = 1;
            end
        end
        BEQ: begin
            if(rf_rp0_data == 0) begin
                pc_load = 1;
                pc_load_inr = 1;
            end
        end
        ADD,
        SUB,
        AND,
        OR,
        XOR,
        LSL,
        LSR,
        SLT: begin
            rf_wp_en = 1;
        end
        LD: begin
            rf_wp_addr = instruction[11:8];
            rf_wp_data = {8'b0, imm};
            rf_wp_en = 1;
        end
        LDU: begin
            rf_wp_data = {imm, 8'b0};
        end
        STM: begin
            mem_wr = 1;
            mem_addr = rf_rp0_data;
            mem_out = rf_rp1_data;
        end
        LDM: begin
            mem_rd = 1;
            rf_wp_en = 1;
            rf_wp_data = mem_y;
            mem_addr = rf_rp0_data;
        end
    endcase
end

endmodule
