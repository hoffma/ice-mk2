#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "edsasm.h"


extern int verbose;

struct variable vtab[4096];
struct label    ltab[4096];
struct instr    itab[4096];

static int current_address = 0;

int vcnt = 0;
int lcnt = 0;
int icnt = 0;


static struct token toktab[] = {

  { TOK_INSTR_ADD,     "add",       GRP_ARITH,  ARITH_ADD,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_SUB,     "sub",       GRP_ARITH,  ARITH_SUB,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_INC,     "inc",       GRP_ARITH,  ARITH_INC,    OP_DST | OP_SRC0            },
  { TOK_INSTR_DEC,     "dec",       GRP_ARITH,  ARITH_DEC,    OP_DST | OP_SRC0            },
  

  { TOK_INSTR_AND,     "and",       GRP_LOGIC,  LOGIC_AND,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_OR,      "or",        GRP_LOGIC,  LOGIC_OR,     OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_NAND,    "nand",      GRP_LOGIC,  LOGIC_NAND,   OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_NOR,     "nor",       GRP_LOGIC,  LOGIC_NOR,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_XOR,     "xor",       GRP_LOGIC,  LOGIC_XOR,    OP_DST | OP_SRC0 | OP_SRC1  }, 
  { TOK_INSTR_XNOR,    "xnor",      GRP_LOGIC,  LOGIC_XNOR,   OP_DST | OP_SRC0 | OP_SRC1  },  
  { TOK_INSTR_NOT,     "not",       GRP_LOGIC,  LOGIC_NOT,    OP_DST | OP_SRC0            },
  

  { TOK_INSTR_SL,      "sl",        GRP_SHIFT,  SHIFT_SL,     OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_LSR,     "lsr",       GRP_SHIFT,  SHIFT_LSR,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_ASR,     "asr",       GRP_SHIFT,  SHIFT_ASR,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_ROL,     "rol",       GRP_SHIFT,  SHIFT_ROL,    OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_ROR,     "ror",       GRP_SHIFT,  SHIFT_ROR,    OP_DST | OP_SRC0 | OP_SRC1  },
  

  { TOK_INSTR_LD,      "ld",        GRP_MEM,    MEM_LDM,      OP_DST | OP_IMM             },
  { TOK_INSTR_LDM,     "ldm",       GRP_MEM,    MEM_LDM,      OP_DST | OP_SRC0            },
  { TOK_INSTR_STM,     "stm",       GRP_MEM,    MEM_STM,      OP_SRC0 | OP_SRC1           },
  { TOK_INSTR_LDM,     "ldm.i",     GRP_MEM,    MEM_LDM,      OP_DST | OP_IMM             },
  { TOK_INSTR_STM,     "stm.i",     GRP_MEM,    MEM_STM,      OP_DST | OP_IMM             },

  { TOK_INSTR_BRA,     "bra",       GRP_BRA,    BRA_BRA,      OP_IMM                      },
  { TOK_INSTR_BRT,     "brt",       GRP_BRA,    BRA_BRT,      OP_DST | OP_IMM             },
  { TOK_INSTR_BRF,     "brf",       GRP_BRA,    BRA_BRF,      OP_DST | OP_IMM             },


  { TOK_INSTR_CMPLT,   "clt",       GRP_CMP,    CMP_LT,       OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPLEQ,  "cleq",      GRP_CMP,    CMP_LEQ,      OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPLT,   "cgt",       GRP_CMP,    CMP_GT,       OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPGEQ,  "clt",       GRP_CMP,    CMP_GEQ,      OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPEQ,   "ceq",       GRP_CMP,    CMP_EQ,       OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPNEQ,  "cneq",      GRP_CMP,    CMP_NEQ,      OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPZ,    "cz",        GRP_CMP,    CMP_Z,        OP_DST | OP_SRC0 | OP_SRC1  },
  { TOK_INSTR_CMPNZ,   "cnz",       GRP_CMP,    CMP_NZ,       OP_DST | OP_SRC0 | OP_SRC1  },


  /* pseudo instructions */

  { TOK_ORG_ORIGIN,    ".origin",    -1, -1, -1 },
  { TOK_ORG_LABEL,     ".label",     -1, -1, -1 },
  { TOK_ORG_PROG,      ".prog",      -1, -1, -1 },
  { TOK_ORG_DATA,      ".data",      -1, -1, -1 },
  { TOK_ORG_VARIABLE,  ".variable",  -1, -1, -1 },
  { TOK_ORG_STRING,    ".string",    -1, -1, -1 },

  {  -1, NULL, -1, -1, -1 },
};


static struct token regtab[] = {
  { TOK_RF_R0,         "r0",          0, -1, -1 },
  { TOK_RF_R1,         "r1",          1, -1, -1 },
  { TOK_RF_R2,         "r2",          2, -1, -1 },
  { TOK_RF_R3,         "r3",          3, -1, -1 },
  { TOK_RF_R4,         "r4",          4, -1, -1 },
  { TOK_RF_R5,         "r5",          5, -1, -1 },
  { TOK_RF_R6,         "r6",          6, -1, -1 },
  { TOK_RF_R7,         "r7",          7, -1, -1 },
  { TOK_RF_R8,         "r8",          8, -1, -1 },
  { TOK_RF_R9,         "r9",          9, -1, -1 },
  { TOK_RF_R10,        "r10",        10, -1, -1 },
  { TOK_RF_R11,        "r11",        11, -1, -1 },
  { TOK_RF_R12,        "r12",        12, -1, -1 },
  { TOK_RF_R13,        "r13",        13, -1, -1 },
  { TOK_RF_R14,        "r14",        14, -1, -1 },
  { TOK_RF_R15,        "r15",        15, -1, -1 },

  {  -1, NULL, -1, -1, -1 },
};


int findRegister (char *tok)
{
  int r;
  struct token *tt;

  for (tt = regtab; tt->tok != NULL; tt++) 
    if (!strcasecmp(tt->tok, tok)) 
      return tt->param;
   
  return -1;
}


int findVariable (char *v) 
{
  int i;

  if (v == NULL)
    return -1;

  for (i = 0; i < vcnt; i++) 
    if (!strcasecmp(v, vtab[i].ident))
      return vtab[i].addr;

  return -1;
}


int findLabel (char *l) 
{
  int i;

  if (l == NULL)
    return -1;

  for (i = 0; i < lcnt; i++) 
    if (!strcasecmp(l, ltab[i].ident))
      return ltab[i].addr;

  return -1;
}

/* ------------------------------------------------- */

int identifyToken (char *tok, int *p, int *p1, int *p2) 
{
  struct token *tt;

  for (tt = toktab; tt->tok != NULL; tt++) 
    if (!strcasecmp(tt->tok, tok)) {
      if (p)
	*p = tt->param;

     if (p1)
	*p1 = tt->p1;

     if (p2)
	*p2 = tt->p2;

      return tt->id;
    }

  if (p)
    *p = -1;

  if (p1)
    *p1 = -1;

  if (p2)
    *p2 = -1;


  return -1;
}

/* ------------------------------------------------- */

void parse_variable (void)
{
  int v = 0;
  char *arg, *val;

  if ((arg = strtok(NULL, " \t\n")) == NULL) {
    fprintf(stderr,"%% FATAL ERROR : Failed to parse variable identifier !\n");
    exit(EXIT_FAILURE);
  }

  if (arg[0] != '%') {
    fprintf(stderr,"%% FATAL ERROR : Illegal variable identifier \"%s\" !\n", arg);
    exit(EXIT_FAILURE);
  }
  
  if ((val = strtok(NULL, " \t\n")) != NULL) {
    sscanf(val, "%i", &v);
    if (verbose)
      printf ("%% variable #%d %s at 0x%03x, value = 0x%x\n", vcnt, arg, current_address, v);
  }

  vtab[vcnt].ident = strdup(arg);
  vtab[vcnt].addr = current_address;
  vtab[vcnt].value = v;
  vcnt++;
  current_address;
}


void parse_3addr (int *dst, int *src0, int *src1)
{
  int v = 0;
  char *arg;

  if ((arg = strtok(NULL, " \t\n")) == NULL) {
    fprintf(stderr,"%% FATAL ERROR : Failed to parse destination register address !\n");
    exit(EXIT_FAILURE);
  }

  if (ptok(arg, dst, NULL, NULL) < 0) {
    fprintf(stderr,"%% FATAL ERROR : Failed to parse destination register \"%s\" !\n", arg);
    exit(EXIT_FAILURE);
  }

  if ((arg = strtok(NULL, " \t\n")) == NULL) {
    fprintf(stderr,"%% FATAL ERROR : Failed to parse source 0 register address !\n");
    exit(EXIT_FAILURE);
  }

  if (ptok(arg, dst, NULL, NULL) < 0) {
    fprintf(stderr,"%% FATAL ERROR : Failed to parse source 0 register \"%s\" !\n", arg);
    exit(EXIT_FAILURE);
  }

  arg = strtok(NULL, " \t\n");
  ptok(arg, src1, NULL, NULL);
}


void parseOrigin (void)
{
  char *arg;
  int addr;

  arg = strtok(NULL, " \t\n");
  sscanf(arg, "%i", &addr);
  if (verbose) 
    printf ("%% setting origin to 0x%03x\n", addr);
  current_address = addr;
}


void parseLabelDecl (void)
{
  char *arg;

  arg = strtok(NULL, " \t\n");
  if (verbose)
    printf ("%% label #%d %s at 0x%03x \n", lcnt, arg, current_address);
  ltab[lcnt].ident = strdup(arg);
  ltab[lcnt].addr = current_address;
}


void parseVariableDecl (void)
{
  char *arg;
  char *val;
  int v;

  arg = strtok(NULL, " \t\n");

  if ((val = strtok(NULL, " \t\n")) != NULL) {
    sscanf(val, "%i", &v);
    if (verbose)
      printf ("%% variable #%d %s at 0x%03x, value = 0x%x\n", vcnt, arg, current_address, v);
  }
  else {
    if (verbose)
      printf ("%% variable #%d %s at 0x%03x, uninitialized value, assuming 0x0\n", vcnt, arg, current_address);
    v = 0;
  }

  vtab[current_address].ident = strdup(arg);
  vtab[current_address].addr = current_address;
  vtab[current_address].value = v;
  vcnt++;
  current_address++;
}


void parseStringDecl (void)
{
  char *arg, *str;

  arg = strtok(NULL, " \t\n");
	
  //if (verbose)
  printf ("%% string variable #%d %s at 0x%03x\n", vcnt, arg, current_address);

  str = strtok(NULL, "\"");
  printf ("%% string = %s\n", str);
}


/* ------------------------------------------------- */


void instr_set_addr (int addr) 
{
  current_address = addr;
}


void instr_clear (void)
{
  
}


void instr_encode (void) 
{
  int i;

  for (i = 0; i < 4096; i++) {
    if (itab[i].addr == i) {
      if (itab[i].imm_flag) {
	itab[i].value = 
	  ((0x7   & itab[i].grp) << 19) | 
	  ((0xf   & itab[i].opc) << 16) | 
	  ((0xf   & itab[i].dst) << 12) | 
	  ((0xfff & itab[i].imm));
      }
      else {
	itab[i].value = 
	  ((0x7 & itab[i].grp)  << 19) | 
	  ((0xf & itab[i].opc)  << 16) | 
	  ((0xf & itab[i].src0) << 12) |
	  ((0xf & itab[i].src1) <<  8) |
	  ((0xf & itab[i].dst));
      }
    }
  }
}


/* ------------------------------------------------- */

void tobitstring (char *buf, int len, int value)
{
  int i, j; 

  if (!buf)
    return;

  if (len <= 0)
    return;

  for (j = 0, i = len - 1; j < len; i--, j++) 
    if (value & (1 << i))
      buf[j] = '1';
    else 
      buf[j] = '0';
  
  buf[len] = (char)0;
}

