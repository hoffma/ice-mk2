#include <verilatedos.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include "verilated.h"
#include "Vtop.h"
#include "testb.h"

int	main(int argc, char **argv) {
	Verilated::commandArgs(argc, argv);
	TESTB<Vtop>	*tb
		= new TESTB<Vtop>;
	tb->opentrace("top.vcd");

    tb->m_core->en = 0;
    tb->m_core->reset = 1;
    tb->tick();
    tb->tick();
    tb->m_core->reset = 0;
    tb->tick();
    tb->tick();
    tb->m_core->en = 1;
    tb->tick();
    tb->tick();
    tb->tick();

    for(int i = 0; i < 20; i++)
        tb->tick();
    tb->m_core->jmp_addr = 0x1234;
    tb->tick();
    tb->m_core->load = 1;
    tb->tick();
    tb->m_core->load = 0;
    
    for(int i = 0; i < 20; i++)
        tb->tick();


    tb->tick();
    
    
	printf("\n\nSimulation complete\n");
}
