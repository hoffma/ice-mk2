#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "iceasm.h"
#include "dbg.h"

int verbose = 1;

#define ROM_SIZE 128

/**
 * TODO: Solve label/variable problem on 1st pass
 */

void add_instr_imm(int curr_addr, int opc, int immediate, char *lbl)
{
    itab[icnt].addr = curr_addr;
    itab[icnt].imm = immediate;
    itab[icnt].opc = opc;
    itab[icnt].regA = -1;
    itab[icnt].regB = -1;
    itab[icnt].regY = -1;
    itab[icnt].var = NULL;
    itab[icnt].label = lbl;
    icnt++;
}

void add_instr_reg_imm(int curr_addr, int opc, int reg, int immediate, char* lbl, char* var)
{
    itab[icnt].addr = curr_addr;
    itab[icnt].imm = immediate;
    itab[icnt].opc = opc;
    itab[icnt].regA = reg;
    itab[icnt].regB = -1;
    itab[icnt].regY = -1;
    itab[icnt].var = var;
    itab[icnt].label = lbl;
    icnt++;
}

void add_instr_3reg(int curr_addr, int opc, int ra, int rb, int ry)
{
    itab[icnt].addr = curr_addr;
    itab[icnt].imm = -1;
    itab[icnt].opc = opc;
    itab[icnt].regA = ra;
    itab[icnt].regB = rb;
    itab[icnt].regY = ry;
    itab[icnt].var = NULL;
    itab[icnt].label = NULL;
    icnt++;
}

void add_instr_2reg_read(int curr_addr, int opc, int ra, int rb)
{
    itab[icnt].addr = curr_addr;
    itab[icnt].imm = -1;
    itab[icnt].opc = opc;
    itab[icnt].regA = ra;
    itab[icnt].regB = rb;
    itab[icnt].regY = 0;
    itab[icnt].var = NULL;
    itab[icnt].label = NULL;
    icnt++;
}

void add_instr_2reg_rw(int curr_addr, int opc, int ra, int ry)
{
    itab[icnt].addr = curr_addr;
    itab[icnt].imm = -1;
    itab[icnt].opc = opc;
    itab[icnt].regA = ra;
    itab[icnt].regB = 0;
    itab[icnt].regY = ry;
    itab[icnt].var = NULL;
    itab[icnt].label = NULL;
    icnt++;
}

int main(int argc, char *argv[])
{
    FILE *file;
    char *tok, *arg;
    char *label, *variable;
    char *cmt;
    char buf[1024];
    int t_id, encoding;
    int imm, ra, rb, ry;
    int match;
    int i;
    int linecount;

    current_address = 0;

    if(argc < 2) {
        fprintf(stderr, "%s: no input file given\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    log_info("## Trying to open input file: %s", argv[1]);

    if((file = fopen(argv[1], "r")) == NULL) {
        perror("fopen error");
        exit(EXIT_FAILURE);
    }

    while (!feof(file)) {
        linecount++;
        /*clear buffer */
        bzero(buf, 1024);
        fgets(buf, 1024, file);
        label = NULL;
        variable = NULL;
        
        if((cmt = strchr(buf, ';')) != NULL) {
            log_info("## Comment: %s", cmt);
            *cmt = (char)0;
            continue;
        }

        if ((tok = strtok(buf, " \t\n")) != NULL) { 
            t_id = identifyToken(tok, &encoding);

            log_info("## token 0x%02x, %s, encoding = %d", t_id, tok, encoding);

            /* decode values */
            switch(encoding) {
                case (OP_IMM):
                    log_info("Immediate only command");
                    arg = strtok(NULL, " \t\n");
                    if((match = sscanf(arg, "%i", &imm)) == 0) {
                        imm = -1;
                        label = strdup(arg);
                    }
                    break;
                case (OP_RA | OP_IMM):
                    log_info("Register and Immediate command");
                    arg = strtok(NULL, " \t\n");
                    if((ra = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    arg = strtok(NULL, " \t\n");
                    if((match = sscanf(arg, "%i", &imm)) == 0) {
                        imm = -1;
                        label = strdup(arg);
                        variable = strdup(arg);
                    }
                    break;
                case (OP_RA):
                    log_info("Single register command");
                    arg = strtok(NULL, " \t\n");
                    if((ra = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    break;
                case (OP_RA | OP_RB | OP_RY):
                    log_info("3 register command");
                    arg = strtok(NULL, " \t\n");
                    if((ry = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    arg = strtok(NULL, " \t\n");
                    if((ra = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    arg = strtok(NULL, " \t\n");
                    if((rb = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    break;
                case (OP_RA | OP_RB):
                    log_info("2 read register command");
                    arg = strtok(NULL, " \t\n");
                    if((ra = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    arg = strtok(NULL, " \t\n");
                    if((rb = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    break;
                case (OP_RA | OP_RY):
                    log_info("1 read, 1 write register command");
                    arg = strtok(NULL, " \t\n");
                    if((ry = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    arg = strtok(NULL, " \t\n");
                    if((ra = findRegister(arg)) == -1) {
                        fprintf(stderr, "Error finding register %s, line %d\n", arg, linecount);
                        return 1;
                    }
                    break;
            }

            /* add instructions to instruction tab */
            switch(t_id) {
                case TOK_ORG_LABEL:
                    //parse_variable();
                    parse_label();
                    break;
                    
                case TOK_ORG_VARIABLE:
                    parse_variable();
                    break;

                case TOK_ORG_ALIAS:
                    break;

                case TOK_ORG_ORIGIN:
                    break;

                case TOK_ORG_PROG:
                    break;

                case TOK_ORG_DATA:
                    break;

                case TOK_INSTR_JAL:
                    add_instr_imm(current_address, t_id, imm, label);
                    current_address++;
                    break;
                    
                case TOK_INSTR_JR:
                    add_instr_reg_imm(current_address, t_id, ra, 0, NULL, NULL);
                    current_address++;
                    break;

                case TOK_INSTR_BNE:
                case TOK_INSTR_BEQ:
                    add_instr_reg_imm(current_address, t_id, ra, imm, label, NULL);
                        current_address++;
                        break;
                case TOK_INSTR_LD:
                case TOK_INSTR_LDU:
                    add_instr_reg_imm(current_address, t_id, ra, imm, NULL, variable);
                    current_address++;
                    break;

                case TOK_INSTR_ADD:
                case TOK_INSTR_SUB:
                case TOK_INSTR_AND:
                case TOK_INSTR_OR:
                case TOK_INSTR_XOR:
                case TOK_INSTR_LSL:
                case TOK_INSTR_LSR:
                case TOK_INSTR_SLT:
                    add_instr_3reg(current_address, t_id, ra, rb, ry);
                    current_address++;
                    break;

                case TOK_INSTR_STM:
                    add_instr_2reg_read(current_address, t_id, ra, rb);
                    current_address++;
                    break;

                case TOK_INSTR_LDM:
                    add_instr_2reg_rw(current_address, t_id, ra, ry);
                    current_address++;
                    break;

                default:
                    fprintf(stderr, "Error parsing token: %s\n", tok);
                    return 1;
            }
        }
    }
    fclose(file);

    
    /* resolve variables */
    for(i = 0; i < TAB_SIZE; i++) {
        int v, l;
        if(itab[i].var != NULL) {
            if((v = findVariable(itab[i].var)) >= 0) {
                log_info("## resolved variable %s at 0x%03x\n", itab[v].var, v);
                itab[i].imm = v;
            }
        }
        if(itab[i].label != NULL) {
            if((l = findLabel(itab[i].label)) >= 0) {
                log_info("## resolved label %s at 0x%03x\n", itab[l].label, l);
                itab[i].imm = l;
            }
        }
    }

    for(i = 0; i < icnt; i++) {
        printf("#0x%03x: opc: 0x%x, reg a, b, y: %d, %d, %d; imm: %d, var: %s, lbl: %s\n",
        itab[i].addr, itab[i].opc, itab[i].regA, itab[i].regB, itab[i].regY, itab[i].imm, itab[i].var, itab[i].label);
    }
    
    encode_instructions();
    
    FILE *fPtr;
    fPtr = fopen("ice_rom.vhdl", "w");

    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file.\n");
        exit(EXIT_FAILURE);
    }
    
    fprintf(fPtr, "library IEEE;\n");
    fprintf(fPtr, "use IEEE.STD_LOGIC_1164.ALL;\n");
    fprintf(fPtr, "\n");
    fprintf(fPtr, "package imem is\n");
    fprintf(fPtr, "type mem is array ( 0 to %d) of std_logic_vector(15 downto 0);\n", ROM_SIZE-1);
    fprintf(fPtr, "\n");
    fprintf(fPtr, "constant imem_rom : mem := (\n");

    for(i = 0; i < ROM_SIZE; i++) {
        to_bitstring(buf, 16, itab[i].binValue);
        fprintf(fPtr, "%d => \"%s\"", i, buf);
        if(i < ROM_SIZE-1)
            fprintf(fPtr, ",\n");
        else
            fprintf(fPtr, "\n");
    }

    fprintf(fPtr, ");\n");
    fprintf(fPtr, "end imem;\n");
    
    /*for(i = 0; i < 32; i++) {
        printf("%04x\n", itab[i].binValue&0xffff);
    }*/

    return 0;
}
