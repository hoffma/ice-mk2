----------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ice_pc is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           load : in  STD_LOGIC;
           load_addr : in  STD_LOGIC_VECTOR (15 downto 0);
           pc : out  STD_LOGIC_VECTOR (15 downto 0));
end ice_pc;

architecture Behavioral of ice_pc is

signal current_pc : STD_LOGIC_VECTOR(15 downto 0)  := (others => '0');

begin
	process(clk, reset)
	begin
        if reset = '1' then
                current_pc <= (others => '0');
        elsif clk'event and clk = '1' then
            if load = '1' then
                current_pc <= load_addr;
            else
                current_PC <= STD_LOGIC_VECTOR(unsigned(current_pc) + 1);
            end if;
        end if;
	end process;
	
	pc <= current_pc; 
	
end Behavioral;

