.const $one 0x1

ld r1 $one
add r1 r2 r3
nop
jal 0x1234
jal %delay
ld r0 15
nop
.label loop
    j 0b101001010
    brnz r5 0x1234
j %loop
ldm r1 r2
stm r3 r4

.label delay
    addi r0 $one
    jr r7

