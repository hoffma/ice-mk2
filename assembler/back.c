#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <libgen.h>

#include "edsasm.h"

#define DEBUG

int verbose = 1;


int isImmediate (char *tok)
{
    if (tok == NULL)
        return -1;

    if (*tok == '%')
        return -1;

    return 0;
}


int isRegister (char *tok)
{
    if (tok == NULL)
        return 0;

    return findRegister(tok);
}


int isVariable (char *tok)
{
    if (tok == NULL)
        return -1;

    if (*tok != '%') 
        return -1;

    return 0;
}


void add_instr_imm (int addr, int grp, int opc, int dest, int imm) 
{
    itab[icnt].addr     = addr;
    itab[icnt].imm_flag = 1;
    itab[icnt].imm      = imm;
    itab[icnt].label    = NULL;
    itab[icnt].var      = NULL;
    itab[icnt].grp      = grp;
    itab[icnt].opc      = opc;
    itab[icnt].dst      = dest;
    itab[icnt].src0     = -1;
    itab[icnt].src1     = -1;
    icnt++;
} 


void add_instr_3addr (int addr, int grp, int opc, int dest, int src0, int src1) 
{
    itab[icnt].addr     = addr;
    itab[icnt].imm_flag = 0;
    itab[icnt].imm      = -1;
    itab[icnt].label    = NULL;
    itab[icnt].var      = NULL;
    itab[icnt].grp      = grp;
    itab[icnt].opc      = opc;
    itab[icnt].dst      = dest;
    itab[icnt].src0     = src0;
    itab[icnt].src1     = src1;
    icnt++;
}


int main (int ac, char **av)
{
    char buf[1024];
    char basename[1024], imem_name[1024], dmem_name[1024];
    char *tok, *arg, *val;
    int addr;
    int param, p1, p2;
    int dst, src0, src1, src2;
    int current_address = 0;
    int v, l, i, t;
    char *rem, *dot, *str;
    FILE *file, *imem_file, *dmem_file;
    struct stat st;


    /* parse asembler file name from command line, stat and open... */

    if (ac < 2) {
        fprintf(stderr,"%s: no input file\n", av[0]);
        exit(EXIT_FAILURE);
    }

    if (verbose) {
        fprintf(stderr, "%% trying to open input file %s\n", av[ac-1]);
    }

    if (stat(av[ac-1], &st) < 0) {
        fprintf(stderr,"%s: failed to stat input file %s\n", av[0], av[ac-1]);
        exit(EXIT_FAILURE);
    }

    if ((file = fopen(av[ac-1], "r")) == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }


    /* assemble output filenames for data and instruction memory */

    strcpy(basename, av[ac-1]);
    if ((dot = strrchr(basename, '.')) != NULL) {
        *dot = 0;
    }

    if (verbose)
        fprintf(stderr, "%% basename = %s\n", basename);

    strcpy(imem_name, basename);
    strcat(imem_name, "_imem.coe");
    strcpy(dmem_name, basename);
    strcat(dmem_name, "_dmem.coe");

    if (verbose) {
        fprintf(stderr, "%% imem_name = %s\n", imem_name);
        fprintf(stderr, "%% dmem_name = %s\n", dmem_name);
    }


    /* open instruction and data memory files for writing */

    if ((imem_file = fopen(imem_name, "w")) == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }  

    if ((dmem_file = fopen(dmem_name, "w")) == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }  



    /* parse assembler file - FIRST PASS */

    while (!feof(file)) {
        bzero(buf, 1024);
        fgets(buf, 1024, file);

        /* remove comments from buffer */

        if ((rem = strchr(buf, '#')) != NULL) {
            if (verbose) 
                fprintf (stderr, "%% comment %s", rem);
            *rem = (char)0;
        }

        if ((tok = strtok(buf, " \t\n")) != NULL) { 

            t = identifyToken(tok, &param, &p1, &p2);

            printf ("t = %d, tok = %s, p2 = %d\n", t, tok, p2);

            if (p2 > 0) {
                if (p2 & OP_DST) {
                    printf ("  destination register expected\n");
                }
                if (p2 & OP_SRC0) {
                    printf ("  source 0 register expected\n");
                }
                if (p2 & OP_SRC1) {
                    printf ("  source 1 register expected\n");
                }  
            }


            switch (t) {

                /* swicht between data and text mode */

                case TOK_ORG_DATA :
                case TOK_ORG_PROG :
                    break;

                    /* VARIABLES, ORIGINS AND LABELS */

                case TOK_ORG_ORIGIN :
                    arg = strtok(NULL, " \t\n");
                    sscanf(arg, "%i", &addr);
                    if (verbose) 
                        printf ("%% setting origin to 0x%03x\n", addr);
                    current_address = addr;
                    break;

                case TOK_ORG_LABEL :
                    arg = strtok(NULL, " \t\n");
                    if (verbose)
                        printf ("%% label #%d %s at 0x%03x \n", lcnt, arg, current_address);
                    ltab[lcnt].ident = strdup(arg);
                    ltab[lcnt].addr = current_address;
                    lcnt++;
                    break;

                case TOK_ORG_VARIABLE :
                    arg = strtok(NULL, " \t\n");
                    if ((val = strtok(NULL, " \t\n")) != NULL) {
                        sscanf(val, "%i", &v);
                        if (verbose)
                            printf ("%% variable #%d %s at 0x%03x, value = 0x%x\n", vcnt, arg, current_address, v);
                    }
                    else {
                        if (verbose)
                            printf ("%% variable #%d %s at 0x%03x, uninitialized value, assuming 0x0\n", vcnt, arg, current_address);
                        v = 0;
                    }

                    vtab[current_address].ident = strdup(arg);
                    vtab[current_address].addr = current_address;
                    vtab[current_address].value = v;
                    vcnt++;
                    current_address++;
                    break;

                case TOK_ORG_STRING :
                    arg = strtok(NULL, " \t\n");

                    //if (verbose)
                    printf ("%% string variable #%d %s at 0x%03x\n", vcnt, arg, current_address);

                    str = strtok(NULL, "\"");

                    printf ("%% string = %s\n", str);
                    break;


                    /* INSTRUCTIONS */

                case TOK_INSTR_INC :
                case TOK_INSTR_DEC :

                    break;

                case TOK_INSTR_ADD :
                case TOK_INSTR_SUB :
                case TOK_INSTR_CMPLT :
                case TOK_INSTR_CMPGT :
                case TOK_INSTR_CMPLEQ :
                case TOK_INSTR_CMPGEQ :
                case TOK_INSTR_CMPEQ :
                case TOK_INSTR_CMPNEQ :
                case TOK_INSTR_CMPZ :
                case TOK_INSTR_CMPNZ :
                    break;

                case TOK_INSTR_BRA :
                    break;

                case TOK_INSTR_BRT :
                case TOK_INSTR_BRF :
                    break;

                case TOK_INSTR_LD :
                case TOK_INSTR_LDM :
                case TOK_INSTR_STM :
                    break;


                default :
                    fprintf(stderr,"%% ERROR: Unknown token %s encountered!\n", tok);
                    exit(EXIT_FAILURE);
                    break;
            }
        }
    }

    fclose(file);


    /* resolve variable identifiers */

    for (i = 0; i < 4096; i++) {
        if (itab[i].imm_flag && itab[i].imm == -1) {
            if (itab[i].var != NULL) {
                if ((v = findVariable(itab[i].var)) >= 0) {
                    if (verbose)
                        printf ("%% resolved variable %s at 0x%03x\n", itab[v].var, v);
                    itab[i].imm = v;
                }
                else {
                    fprintf(stdout, "ERROR: failed to resolve variable %s !\n", itab[i].var);
                    exit(EXIT_FAILURE);
                }
            }
            else if (itab[i].label != NULL) {
                if ((l = findLabel(itab[i].label)) >= 0) {
                    if (verbose) 
                        printf ("%% resolved label %s at 0x%03x\n", itab[i].label, l);
                    itab[i].imm = l;
                }
                else {
                    fprintf(stdout, "ERROR: failed to resolve label %s !\n", itab[i].label);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
    for (i = 0; i < 32; i++) {
        tobitstring(buf, 24, itab[i].value);
        puts(buf);
    }


    /* encode instructions */

    instr_encode();
    /*
       [> dump instructions memory <]

       fprintf(imem_file,"; Sample memory initialization file for Single Port Block Memory,\n");
       fprintf(imem_file,"; v3.0 or later.\n");
       fprintf(imem_file,";\n");
       fprintf(imem_file,"; This .COE file specifies initialization values for a block \n");
       fprintf(imem_file,"; memory of depth=16, and width=8. In this case, values are \n");
       fprintf(imem_file,"; specified in hexadecimal format.\n");
       fprintf(imem_file,"memory_initialization_radix=16;\n");
       fprintf(imem_file,"memory_initialization_vector=\n");

       for (i = 0; i < 4096; i++) {
       tobitstring(buf, 24, itab[i].value);
       puts(buf);
       }

       puts("");

       [> dump data memory <]

       for (i = 0; i < 4096; i++) {
       if (vtab[i].addr == i) {
       fprintf(dmem_file, "%04x\n", vtab[i].value);

       tobitstring(buf, 16, vtab[i].value);
       puts(buf);
       }
       else {
       fprintf(dmem_file, "0000\n");
       tobitstring(buf, 16, 0);
    //puts(buf);
    }
    }*/

    fclose (imem_file);
    fclose (dmem_file);
}
