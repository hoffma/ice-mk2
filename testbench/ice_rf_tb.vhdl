----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 02.06.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_rf_tb is
end ice_rf_tb;


architecture Behavioral of ice_rf_tb is

    signal clk : STD_LOGIC := '0';
    signal s_rp0_addr, s_rp1_addr, s_wp_addr : std_logic_vector(2 downto 0);
    signal s_rp0_data, s_rp1_data, s_wp_data : std_logic_vector(15 downto 0);
    signal s_wp_en : std_logic;

begin
    dut : entity work.ice_rf port map(clk => clk,
                                     reset => '0',
                                     rp0_addr => s_rp0_addr,
                                     rp0_data => s_rp0_data,
                                     rp1_addr => s_rp1_addr,
                                     rp1_data => s_rp1_data,
                                     wp_addr => s_wp_addr,
                                     wp_data => s_wp_data,
                                     wp_en => s_wp_en);

    clk <= not clk after 5 ns;

    clock_soc: process
    begin
        s_rp0_addr <= "000";
        s_rp1_addr <= "000";
        s_wp_addr <= "000";
        s_wp_data <= x"0001";
        s_wp_en <= '1';
        wait for 100 ns;
        s_wp_addr <= "001";
        s_wp_data <= x"0002";
        wait for 100 ns;
        s_wp_addr <= "010";
        s_wp_data <= x"0003";
        wait for 100 ns;
        s_wp_en <= '0';
        wait for 100 ns;
        s_rp0_addr <= "000";
        s_rp1_addr <= "001";
        wait for 100 ns;
        s_rp0_addr <= "001";
        s_rp1_addr <= "010";
        wait for 100 ns;
    end process;

end Behavioral;

