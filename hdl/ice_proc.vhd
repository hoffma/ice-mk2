----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ice_proc is
    generic (data_width : positive := 16;
            addr_width : positive := 16;
            word_width : positive := 16);
    port(
        clk         : in STD_LOGIC;
        clk90       : in STD_LOGIC;
        reset       : in STD_LOGIC;

        instr_addr  : out STD_LOGIC_VECTOR(11 downto 0);
        instruction : in STD_LOGIC_VECTOR(word_width-1 downto 0);

        mem_addr    : out STD_LOGIC_VECTOR(15 downto 0);
        mem_in      : in STD_LOGIC_VECTOR(data_width-1 downto 0);
        mem_out     : out STD_LOGIC_VECTOR(data_width-1 downto 0);
        mem_rd      : out STD_LOGIC;
        mem_wr      : out STD_LOGIC

        -- interrupt   : in STD_LOGIC_VECTOR(7 downto 0);
        -- interrupt_ack : out STD_LOGIC_VECTOR(7 downto 0)
        );
end ice_proc;

architecture Behavioral of ice_proc is

    constant C_NOOP : std_Logic_Vector(4 downto 0) := "00000";

    constant C_J    : std_logic_vector(4 downto 0) := "00001";
    constant C_JAL  : std_logic_vector(4 downto 0) := "00010";
    constant C_JR   : std_logic_vector(4 downto 0) := "00011";
    constant C_BRNZ : std_logic_vector(4 downto 0) := "00100";
    constant C_BRZ  : std_logic_vector(4 downto 0) := "00101";
    
    constant C_ADD  : std_logic_vector(4 downto 0) := "00110";
    constant C_SUB  : std_logic_vector(4 downto 0) := "00111";
    constant C_MOD  : std_logic_vector(4 downto 0) := "01000";
    constant C_AND  : std_logic_vector(4 downto 0) := "01001";
    constant C_OR   : std_logic_vector(4 downto 0) := "01010";
    constant C_XOR  : std_logic_vector(4 downto 0) := "01011";
    constant C_LSL  : std_logic_vector(4 downto 0) := "01100";
    constant C_LSR  : std_logic_vector(4 downto 0) := "01101";
    constant C_NOT  : std_logic_vector(4 downto 0) := "01110";
    constant C_SLT  : std_logic_vector(4 downto 0) := "01111";

    constant C_ADDI : std_logic_vector(4 downto 0) := "10000";
    constant C_SUBI : std_logic_vector(4 downto 0) := "10001";
    constant C_ANDI : std_logic_vector(4 downto 0) := "10010";

    constant C_LD   : std_logic_vector(4 downto 0) := "11100";
    constant C_LDU  : std_logic_vector(4 downto 0) := "11101";
    constant C_STM  : std_logic_vector(4 downto 0) := "11110";
    constant C_LDM  : std_logic_vector(4 downto 0) := "11111";

    -- registerfile signals
    signal rf_rp0_addr : std_logic_vector(2 downto 0);
    signal rf_rp1_addr : std_logic_vector(2 downto 0);
    signal rf_wp_addr : std_logic_vector(2 downto 0);
    signal rf_rp0_data : std_logic_vector(data_width-1 downto 0);
    signal rf_rp1_data : std_logic_vector(data_width-1 downto 0);
    signal rf_wp_data : std_logic_vector(data_width-1 downto 0);
    signal rf_wp_en : std_logic;

    signal alu_y : std_logic_vector(data_width-1 downto 0);
    signal mem_y : std_logic_vector(data_width-1 downto 0);

    signal pc_pc : std_logic_vector(addr_width-1 downto 0);
    signal pc_load : std_logic;
    signal pc_load_addr : std_logic_vector(addr_width-1 downto 0);

    signal inst_opc : std_logic_vector(4 downto 0);

    signal imm : std_logic_vector(addr_width-1 downto 0);
    

    signal status_reg : std_logic_vector(7 downto 0);

    signal cmp_nz : std_logic;

begin
    rf_rp0_addr <= instruction(10 downto 8);
    rf_rp1_addr <= instruction(7 downto 5);
    mem_y <= mem_in;
    imm <= "00000" & instruction(10 downto 0);
    inst_opc <= instruction(15 downto 11);

    cmp_nz <= '0' when rf_rp0_data = x"0000" else '1';

    instr_addr <= pc_pc(11 downto 0);

    inst_pc : entity work.ice_pc port map(clk => clk,
                                        reset => reset,
                                        load => pc_load,
                                        load_addr => pc_load_addr,
                                        pc => pc_pc);

    inst_rf : entity work.ice_rf port map(clk => clk90,
                                        reset => reset,
                                        rp0_addr => rf_rp0_addr,
                                        rp0_data => rf_rp0_data,
                                        rp1_addr => rf_rp1_addr,
                                        rp1_data => rf_rp1_data,
                                        wp_addr => rf_wp_addr,
                                        wp_data => rf_wp_data,
                                        wp_en => rf_wp_en);

    isnt_alu : entity work.ice_alu port map(a => rf_rp0_data,
                                        b => rf_rp1_data,
                                        imm => imm(7 downto 0),
                                        y => alu_y,
                                        c => status_reg(0),
                                        opc => inst_opc);


    process(instruction, inst_opc, imm, alu_y, mem_y, cmp_nz, rf_wp_addr, rf_rp0_data, rf_rp1_data, rf_wp_data)
        variable TMP : std_logic_vector(15 downto 0) := (others => '0');
    begin
        pc_load_addr <= imm;
        pc_load <= '0';
        rf_wp_en <= '0';
        rf_wp_data <= alu_y;
        rf_wp_addr <= instruction(4 downto 2);
        mem_rd <= '0';
        mem_wr <= '0';
        mem_addr <= (others => '0');
        mem_out <= (others => '0');

        case inst_opc is
            when C_J =>
                pc_load <= '1';
            when C_JAL =>
                TMP := std_logic_vector(unsigned(pc_pc) + 1);
                rf_wp_data <= TMP;
                rf_wp_addr <= "111"; -- reg 7
                rf_wp_en <= '1';
                pc_load <= '1';
            when C_JR =>  
                -- TODO: check this
                pc_load_addr <= rf_rp0_data;
                pc_load <= '1';
            when C_BRNZ =>
                if(cmp_nz = '1' ) then
                    TMP := std_logic_vector(signed(pc_pc) + signed(imm(7 downto 0)));
                    pc_load_addr <= TMP;
                    pc_load <= '1';
                end if;
            when C_BRZ =>
                if(cmp_nz = '0') then
                    TMP := std_logic_vector(signed(pc_pc) + signed(imm(7 downto 0)));
                    pc_load_addr <= TMP;
                    pc_load <= '1';
                end if;

            when C_ADD|C_SUB|C_AND|C_OR|C_XOR|C_LSL|C_LSR|C_SLT =>
                rf_wp_en <= '1';

            when C_ADDI|C_SUBI|C_ANDI =>
                rf_wp_en <= '1';
                rf_wp_addr <= rf_rp0_addr;                
            when C_LD =>
                rf_wp_addr <= instruction(10 downto 8);
                rf_wp_data <= x"00" & imm(7 downto 0);
                rf_wp_en <= '1';
            when C_LDU =>
                rf_wp_addr <= instruction(10 downto 8);
                rf_wp_data <= imm(7 downto 0) & x"00";
                rf_wp_en <= '1';
            when C_STM =>
                mem_wr <= '1';
                mem_addr <= rf_rp0_data;
                mem_out <= rf_rp1_data;
            when C_LDM =>
                mem_rd <= '1';
                mem_addr <= rf_rp0_data;
                rf_wp_en <= '1';
                rf_wp_data <= mem_y;
            when others => 
                pc_load <= '0';
        end case;
    end process;
end Behavioral;
