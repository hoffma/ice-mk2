----------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 02.08.2020
----------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ice_reg is
    generic(width : positive := 16);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR(width-1 downto 0);
           q : out STD_LOGIC_VECTOR(width-1 downto 0);
           enable : in STD_LOGIC);
end ice_reg;

architecture Behavioral of ice_reg is

begin
	process(clk, reset)
	begin
        if reset = '1' then
            q <= (others => '0');
        elsif clk'event and clk = '1' then
            if enable = '1' then
                q <= d;
            end if;
        end if;
	end process;
end Behavioral;

