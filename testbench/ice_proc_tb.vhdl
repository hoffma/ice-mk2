----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_proc_tb is
end ice_proc_tb;

architecture Behavioral of ice_proc_tb is

    signal clk : std_logic := '0';
    signal clk90 : std_logic := '0';
    constant clk_period : time := 40 ns;

    signal reset, mem_rd, mem_wr : STD_LOGIC;
    signal inst_addr : STD_LOGIC_VECTOR(11 downto 0);
    signal mem_addr, mem_in, mem_out : STD_LOGIC_VECTOR(15 downto 0);
    signal instruction : STD_LOGIC_VECTOR(15 downto 0);

    type mem is array ( 0 to 31) of std_logic_vector(15 downto 0);
    constant my_Rom : mem := (
        0 => "1110000100000000",
        1 => "1110010100000101",
        2 => "1110011100101010",
        3 => "1000000100000001",
        4 => "1111010100100000",
        5 => "1111111100001000",
        6 => "0000100000000011",        
            7 => "0000000000000000",
            8 => "0000000000000000",
            9 => "0000000000000000",
            10 => "0000000000000000",
            11 => "0000000000000000",
            12 => "0000000000000000",
            13 => "0000000000000000",
            14 => "0000000000000000",
            15 => "0000000000000000",
            16 => "0000000000000000",            
17 => "0000000000000000",
18 => "0000000000000000",
19 => "0000000000000000",
20 => "0000000000000000",
21 => "0000000000000000",
22 => "0000000000000000",
23 => "0000000000000000",
24 => "0000000000000000",
25 => "0000000000000000",
26 => "0000000000000000",
27 => "0000000000000000",
28 => "0000000000000000",
29 => "0000000000000000",
30 => "0000000000000000",
31 => "0000000000000000"
    );
        
begin

    reset <= '1', '0' after 50 ns; --, '1' after 400 ns, '0' after 410 ns;
    -- generate clocks 
    clk_process :process
    begin
        clk <= '0';
        clk90 <= '0';
        wait for clk_period/4;  
        clk <= '1';
        clk90 <= '0';
        wait for clk_period/4;  
        clk <= '1';
        clk90 <= '1';
        wait for clk_period/4;  
        clk <= '0';
        clk90 <= '1';
        wait for clk_period/4;  
    end process;

    dut : entity work.ice_proc port map(clk => clk,
                                        clk90 => clk90,
                                        reset => reset,
                                        instr_addr => inst_addr,
                                        instruction => instruction,
                                        mem_addr => mem_addr,
                                        mem_in => mem_in,
                                        mem_out => mem_out,
                                        mem_rd => mem_rd,
                                        mem_wr => mem_wr);

    process(mem_rd)
    begin
        if mem_rd'event and mem_rd = '1' then
            if mem_addr /= x"0000" then
                mem_in <= x"0007";
            else
                mem_in <= x"0000";
            end if;
        end if;
    end process;

    process (inst_addr)
    begin
        case inst_addr(4 downto 0) is
            when "00000" => instruction <= my_rom(0);
            when "00001" => instruction <= my_rom(1);
            when "00010" => instruction <= my_rom(2);
            when "00011" => instruction <= my_rom(3);
            when "00100" => instruction <= my_rom(4);
            when "00101" => instruction <= my_rom(5);
            when "00110" => instruction <= my_rom(6);
            when "00111" => instruction <= my_rom(7);
            when "01000" => instruction <= my_rom(8);
            when "01001" => instruction <= my_rom(9);
            when "01010" => instruction <= my_rom(10);
            when "01011" => instruction <= my_rom(11);
            when "01100" => instruction <= my_rom(12);
            when "01101" => instruction <= my_rom(13);
            when "01110" => instruction <= my_rom(14);
            when "01111" => instruction <= my_rom(15);
            when "10000" => instruction <= my_rom(16);
            when "10001" => instruction <= my_rom(17);
            when "10010" => instruction <= my_rom(18);
            when "10011" => instruction <= my_rom(19);
            when "10100" => instruction <= my_rom(20);
            when "10101" => instruction <= my_rom(21);
            when "10110" => instruction <= my_rom(22);
            when "10111" => instruction <= my_rom(23);
            when "11000" => instruction <= my_rom(24);
            when "11001" => instruction <= my_rom(25);
            when "11010" => instruction <= my_rom(26);
            when "11011" => instruction <= my_rom(27);
            when "11100" => instruction <= my_rom(28);
            when "11101" => instruction <= my_rom(29);
            when "11110" => instruction <= my_rom(30);
            when "11111" => instruction <= my_rom(31);
            when others => instruction <= x"0000";
        end case;
    end process;

end Behavioral;
