----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_pc_tb is
end ice_pc_tb;

architecture Behavioral of ice_pc_tb is

    signal clk : std_logic := '0';
    signal reset, load : std_logic;
    signal imm, pc : std_logic_vector(15 downto 0);
	
begin

    dut : entity work.ice_pc port map (clk => clk,
                                        reset => reset,
                                        load => load,
                                        load_addr => imm,
                                        pc => pc);

    clk <= not clk after 5 ns;
    reset <= '1', '0' after 30 ns;
    load <= '0', '1' after 300 ns, '0' after 310 ns;
    imm <= x"aaaa", x"0000" after 400 ns;

end Behavioral;
