.label %start
    ld r14 5
    ld r14 5
    ld r13 60

    ld r0 1
    ld r1 1
    ld r2 3

.label %loop
    and r4 r1 r1
    and r5 r2 r2
    jal %mod
    beq r4 %display
    jal %dontdisplay

.label %display
    stm r14 r1
.label %dontdisplay
    ; increment and go back to loop
    add r1 r1 r0
    jal %loop


; calculate modulo r4 = r4 % r5
.label %mod
    ; check r4 < r5. if yes, break.
    slt r6 r4 r5
    beq r6 %sub
    jr r15
    ; not smaller. sub and loop.
.label %sub
    sub r4 r4 r5
    beq r6 %mod
