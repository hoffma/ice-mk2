------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 02.08.2020
------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_rfdecode is
    Port ( addr : in  STD_LOGIC_VECTOR (2 downto 0);
           en : in  STD_LOGIC;
           sel : out  STD_LOGIC_VECTOR (7 downto 0));
end ice_rfdecode;

architecture Behavioral of ice_rfdecode is

begin
	process (addr, en)
	begin
		sel <= (others => '0');
		
		if en = '1' then
			case addr is
			when "000" =>
				sel(0) <= '1';
			when "001" =>
				sel(1) <= '1';
			when "010" =>
				sel(2) <= '1';
			when "011" =>
				sel(3) <= '1';
			when "100" =>
				sel(4) <= '1';
			when "101" =>
				sel(5) <= '1';
			when "110" =>
				sel(6) <= '1';
			when "111" =>
				sel(7) <= '1';
			when others =>
				sel <= (others => '0');
			end case;
		end if;
	end process;


end Behavioral;

