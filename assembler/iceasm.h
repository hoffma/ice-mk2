#pragma once

#include <stdint.h>

/* ------------ TOKEN IDs ------------- */
/* Instruction token ids are the same as the actual OPC! */
#define TOK_INSTR_JAL   0x0
#define TOK_INSTR_JR    0x1
#define TOK_INSTR_BNE   0x2
#define TOK_INSTR_BEQ   0x3
#define TOK_INSTR_ADD   0x4
#define TOK_INSTR_SUB   0x5
#define TOK_INSTR_AND   0x6
#define TOK_INSTR_OR    0x7
#define TOK_INSTR_XOR   0x8
#define TOK_INSTR_LSL   0x9
#define TOK_INSTR_LSR   0xA
#define TOK_INSTR_SLT   0xB
#define TOK_INSTR_LD    0xC
#define TOK_INSTR_LDU   0xD
#define TOK_INSTR_STM   0xE
#define TOK_INSTR_LDM   0xF

/* organisatorial token IDs */
#define TOK_ORG_LABEL   0x10
#define TOK_ORG_ALIAS   0x11
#define TOK_ORG_ORIGIN  0x12
#define TOK_ORG_IMM     0x13
#define TOK_ORG_PROG    0x14
#define TOK_ORG_DATA    0x15
#define TOK_ORG_VARIABLE 0x16

/* register token IDs */
#define TOK_RF_R0       0x20
#define TOK_RF_R1       0x21
#define TOK_RF_R2       0x22
#define TOK_RF_R3       0x23
#define TOK_RF_R4       0x24
#define TOK_RF_R5       0x25
#define TOK_RF_R6       0x26
#define TOK_RF_R7       0x27
#define TOK_RF_R8       0x28
#define TOK_RF_R9       0x29
#define TOK_RF_R10      0x2A
#define TOK_RF_R11      0x2B
#define TOK_RF_R12      0x2C
#define TOK_RF_R13      0x2D
#define TOK_RF_R14      0x2E
#define TOK_RF_R15      0x2F

/* pseudo instructions */
/* return from subroutine -> jr r15 */
#define TOK_PINST_RET   0x30

/* ------------------------------------ */


/* -------- ENCODING OPTIONS ---------- */

#define OP_NULL         0x0
#define OP_IMM          0x1
#define OP_RA           0x2
#define OP_RB           0x4
#define OP_RY           0x8

/* ------------------------------------ */

struct token {
    int id;
    char *tok;
    int enc;
};

struct variable {
    char *ident;
    int addr;
    int value;
};

struct label {
    char *ident;
    int addr;
};

struct instr {
    int addr;
    int imm;
    int opc;
    int regA;
    int regB;
    int regY;
    char *var;
    char *label;
    int16_t binValue;
};

#define TAB_SIZE 4096

extern struct variable vtab[TAB_SIZE];
extern struct label ltab[TAB_SIZE];
extern struct instr itab[TAB_SIZE];

extern int vcnt;
extern int lcnt;
extern int icnt;

extern int current_address;

int findRegister(char *tok);
int findVariable(char *tok);
int findLabel(char *tok);
int identifyToken(char *tok, int *enc);
void parse_variable(void);
void parse_label(void);
void encode_instructions(void);
void to_bitstring(char* buf, int len, int val);