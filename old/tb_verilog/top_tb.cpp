#include <verilatedos.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include "verilated.h"
#include "Vtop.h"
#include "testb.h"

TESTB<Vtop>	*tb;

void tick() {
    tb->m_core->clk0 = 1;
    tb->m_core->clk90 = 0;
    tb->tick();
    tb->m_core->clk0 = 1;
    tb->m_core->clk90 = 1;
    tb->tick();
    tb->m_core->clk0 = 0;
    tb->m_core->clk90 = 1;
    tb->tick();
    tb->m_core->clk0 = 0;
    tb->m_core->clk90 = 0;
    tb->tick();
}

int	main(int argc, char **argv) {
	Verilated::commandArgs(argc, argv);
	tb = new TESTB<Vtop>;

	tb->opentrace("top.vcd");

    tb->m_core->reset = 1;
    tb->m_core->mem_in = 0;
    tb->m_core->instruction = 0b0000000000000000;  //5 in reg 0
    tick();
    tick();
    tb->m_core->reset = 0;
    
    tb->m_core->instruction = 0b1100000000000101;  //5 in reg 0
    tick();
    /*tb->tick();*/
    tb->m_core->instruction = 0b1100000100001001; // 9 in reg 1
    tick();
    tb->m_core->instruction = 0b1100001100101010; // 0x2a in reg 3
    tick();

    tb->m_core->instruction = 0b0100000000010010; // reg2 = reg0 + reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();

    tb->m_core->instruction = 0b0101001000010010; // reg 2 = reg2 - reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();

    tb->m_core->instruction = 0b0110000000010010; // reg 2 = reg0 and reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();

    tb->m_core->instruction = 0b0111000000010010; // reg 2 = reg0 or reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();

    tb->m_core->instruction = 0b1000000000010010; // reg 2 = reg0 xor reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();

    tb->m_core->instruction = 0b1001000000010010; // reg 2 = reg0 << reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();

    tb->m_core->instruction = 0b1010000000010010; // reg 2 = reg0 >> reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();
    
    tb->m_core->instruction = 0b1011000000010010; // reg 2 = reg0 < reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();
    
    tb->m_core->instruction = 0b1011000100000010; // reg 2 = reg0 > reg1
    tick();
    tb->m_core->instruction = 0b1110001100100000; // display reg 2 at 0x2a
    tick();
    
    tick();
    tb->m_core->instruction = 0b0000000000000000; // reg 2 = reg0 > reg1
    tick();
    
	printf("\n\nSimulation complete\n");
}
