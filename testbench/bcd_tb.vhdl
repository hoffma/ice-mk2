----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity bcd_tb is
end bcd_tb;

architecture Behavioral of bcd_tb is

    signal binary : std_logic_vector(15 downto 0);
    signal thousands, hundreds, tens, ones : std_logic_vector(3 downto 0);
	
begin

    dut : entity work.bcd port map (binary => binary,
                                    thousands => thousands,
                                    hundreds => hundreds,
                                    tens => tens,
                                    ones => ones);

    binary <= x"0000", x"0001" after 5 ns, 
    x"0321" after 10 ns, x"0237" after 20 ns, x"00ab" after 30 ns, x"a0cc" after 40 ns, 
    x"0000" after 60 ns;
    
end Behavioral;
