
.const $stackPtr 0x0f
.const $ledAddr 0x05
.const $maxFib 0x27
.const $delayCount 0x7f

; push a and b onto stack
.label start
nop
nop
ld r1 1
ld r6 $stackPtr
stm r6 r1
addi r6 1
stm r6 r1
addi r6 1

.label main
    ld r2 $ledAddr
    jal %peek
    stm r2 r1
    ldu r2 $maxFib ; 0x2700 == 9984
    slt r2 r1 r2
    brnz r2 2 
    j %start
    jal %fib

.label fib
    jal %pop ; pop b
    and r2 r1 r1 ; move r1 into r2
    jal %pop ; pop a into r1
    add r1 r1 r2
    and r3 r1 r1 ; move r1 into r3
    and r1 r2 r2 ; move r2 into r1
    jal %push
    and r1 r3 r3 
    jal %push
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    jal %delay
    j %main



; load top of stack into r1 and return
.label peek
    subi r6 1
    ldm r6 r1
    addi r6 1
    jr r7

; pop top of stack into r1 and clear top
.label pop
    subi r6 1
    ldm r6 r1
    stm r6 r0
    jr r7

; push r1 onto stack and increment
.label push
    stm r6 r1
    addi r6 1
    jr r7

.label delay
    ld r1 0
    ldu r3 $delayCount
.label delayloop
    addi r1 1
    slt r4 r1 r3
    brnz r4 -2
    jr r7

