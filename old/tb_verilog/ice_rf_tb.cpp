#include <verilatedos.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include "verilated.h"
#include "Vtop.h"
#include "testb.h"

int	main(int argc, char **argv) {
	Verilated::commandArgs(argc, argv);
	TESTB<Vtop>	*tb
		= new TESTB<Vtop>;
	tb->opentrace("top.vcd");

    
    tb->m_core->wp_en = 0;
    tb->tick();
    tb->tick();
    tb->tick();
    tb->tick();
    tb->m_core->wp_en = 1;
    for(int i = 0; i < 16; i++) {
        tb->m_core->wp_addr = i;
        tb->m_core->wp_data = (1 << i);
        tb->tick();
    }
    

    for(int i = 0; i < 16; i++) {
        tb->m_core->rp0_addr = i;
        tb->m_core->rp1_addr = 15-i;
        tb->tick();
    }
    
	printf("\n\nSimulation complete\n");
}
