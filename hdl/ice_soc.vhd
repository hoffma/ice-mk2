-------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.imem.all;

entity ice_soc is
    port(
        clk     : in STD_LOGIC;
        clk90   : in STD_LOGIC; 
        reset   : in STD_LOGIC;
        led     : out STD_LOGIC_VECTOR(7 downto 0);

        -- sdram interface
        sdram_clk120 : in std_logic;
        sdram_csn : out std_logic;
        sdram_rasn : out std_logic;
        sdram_casn : out std_logic;
        sdram_wen : out std_logic;
        sdram_a : out std_logic_vector(12 downto 0);
        sdram_ba : out std_logic_vector(1 downto 0);
        sdram_dqm : out std_logic_vector(1 downto 0);
        sdram_d : inout std_logic_vector(15 downto 0)
    );
end ice_soc;

architecture Behavioral of ice_soc is
    signal mem_addr : std_logic_vector(15 downto 0);
    signal mem_out, mem_in : std_logic_vector(15 downto 0);
    signal proc_instr_addr : std_logic_vector(11 downto 0);
    signal proc_instruction : std_logic_vector(15 downto 0);
    signal mem_rd, mem_wr : std_logic;
    
    signal ram_addr : std_logic_vector(23 downto 0);
    signal data_i, data_o : std_logic_vector(15 downto 0);
    signal read_strobe, write_strobe, sys_rdy_o, sys_ack_i : std_logic;

begin 

    inst_proc : entity work.ice_proc port map(clk => clk,
                                        clk90 => clk90,
                                        reset =>  reset,
                                        instr_addr => proc_instr_addr,
                                        instruction => proc_instruction,
                                        mem_addr => mem_addr,
                                        mem_in => mem_in,
                                        mem_out => mem_out,
                                        mem_rd => mem_rd,
                                        mem_wr => mem_wr);

    inst_ram : entity work.ice_ram port map(clk => clk90,
                                        addr => mem_addr(11 downto 0),
                                        din => mem_out,
                                        wen => mem_wr,
                                        dout => mem_in);

    ram : entity work.sdram_pnru
    port map (
        sys_clk => sdram_clk120,
        sys_rd => read_strobe,
        sys_wr => write_strobe,
        sys_rdy => sys_rdy_o,
        sys_ack => sys_ack_i,
        sys_ab => ram_addr,
        sys_di => data_i,
        sys_do => data_o,

        sdr_n_CS_WE_RAS_CAS => sdram_csn & sdram_wen & sdram_rasn & sdram_casn,
        sdr_ba => sdram_ba,
        sdr_ab => sdram_a,
        sdr_db => sdram_d,
        sdr_dqm => sdram_dqm
    );

    process(clk90)
    begin
        if clk90'event and clk90 = '1' then
            if mem_wr = '1' then
                if mem_addr = x"0005" then
                    led <= mem_out(7 downto 0);
                    bcd <= bcd_o;
                end if;                
            end if;
        end if;
    end process;


    proc_instruction <= imem_rom(to_integer(unsigned(proc_instr_addr)));

end Behavioral;
