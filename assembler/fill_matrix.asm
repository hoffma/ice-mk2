ld r1 0


.variable %PIXADDR 9
.variable %CLR_R 10
.variable %CLR_G 11
.variable %CLR_B 12

jal %fillscreen

.label %outerloop
ld r1 0
ld r2 1
ld r5 5

.label %loop
jal %addone
stm r5 r1
jal %delayloop
jal %loop

.label %delayloop
    ld r0 0
    ld r2 1
    ldu r14 127
    .label %innerloop
    add r0 r0 r2
    slt r6 r0 r14
    bne r6 %innerloop
    jr r15


.label %addone
    ld r2 1
    add r1 r1 r2
    jr r15


.label %fillscreen
    ld r3 0
    ld r4 1
    ; 2048 pixels
    ldu r10 8
    ; blue color
    ld r11 0
    ld r12 %PIXADDR
    ld r13 %CLR_R
    stm r13 r3
    ld r13 %CLR_G
    stm r13 r3
    ld r13 %CLR_B
    stm r13 r11
    .label %fillloop
        stm r12 r3
        add r3 r3 r4
        slt r6 r3 r10
        bne r6 %fillloop
    jal %drawtriag

.label %drawtriag
    ld r3 0
    ld r4 1
    ; bottom left pixel
    ld r8 15
    ldu r10 5
    or r10 r10 r8

    
    ld r11 16
    ld r12 %PIXADDR
    stm r12 r8

    ; red color
    ld r13 %CLR_R
    stm r13 r11
    ld r13 %CLR_G
    stm r13 r3
    ld r13 %CLR_B
    stm r13 r3

    ld r3 0 
    ld r4 1
    ld r5 15
    ld r1 63
    .label %draw1
        stm r12 r10
        sub r10 r10 r1
        add r3 r3 r4
        jal %delayloop
        slt r6 r3 r5
        bne r6 %draw1
    
    ld r1 65
    ld r3 0
    .label %draw2
        stm r12 r10
        add r10 r10 r1
        add r3 r3 r4
        jal %delayloop
        slt r6 r3 r5        
        bne r6 %draw2

    ld r5 31
    ld r3 0
    .label %draw3
        stm r12 r10
        sub r10 r10 r4
        add r3 r3 r4
        jal %delayloop
        slt r6 r3 r5
        bne r6 %draw3

    jal %outerloop