.label %start
    ; display address for teh 7 segment display
    ld r14 5
    ld r14 5

    ; load 9999 into r13 for max value
    ld r13 255
    ; ldu r12 39
    ; or r13 r13 r12

    ; r0 contains current number
    ld r0 2

.label %outerloop
    ld r4 1
    add r0 r0 r4
    slt r4 r0 r13
    bne r4 %inrange
    ld r0 2

.label %inrange
    ld r2 2
.label innerloop
    ; r2 >= r0 ==> prime found
    slt r4 r2 r0
    beq r4 %prime
    xor r4 r2 r0
    beq r4 %prime

    or r4 r0 r0
    or r5 r2 r2
    jal %mod
    beq r4 %notprime
    ; not a divisor. increment r2
    ld r4 1
    add r2 r2 r4
    jal %innerloop

.label %prime
    stm r14 r0
    ;jal %delay
.label %notprime
    jal %outerloop

; calculate modulo r4 = r4 % r5
.label %mod
    ; check r4 < r5. if yes, break.
    slt r6 r4 r5
    beq r6 %sub
    jr r15
    ; not smaller. sub and loop.
.label %sub
    sub r4 r4 r5
    beq r6 %mod

;.label %delay
;    ld r1 1
;    ld r2 20
;.label %delayloop
;    sub r2 r2 r1
;    bne r2 %delayloop
;    jr r15