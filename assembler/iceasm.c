#include <string.h>
#include <stdlib.h>

#include "iceasm.h"
#include "dbg.h"

struct variable vtab[TAB_SIZE];
struct label ltab[TAB_SIZE];
struct instr itab[TAB_SIZE];

int current_address = 0;

int vcnt = 0;
int lcnt = 0;
int icnt = 0;

static struct token toktab[] = {
    {TOK_INSTR_JAL, "jal", OP_IMM},
    {TOK_INSTR_JR, "jr", OP_RA},
    {TOK_INSTR_BNE, "bne", OP_RA | OP_IMM},
    {TOK_INSTR_BEQ, "beq", OP_RA | OP_IMM},
    {TOK_INSTR_ADD, "add", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_SUB, "sub", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_AND, "and", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_OR, "or", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_XOR, "xor", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_LSL, "lsl", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_LSR, "lsr", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_SLT, "slt", OP_RA | OP_RB | OP_RY},
    {TOK_INSTR_LD, "ld", OP_RA | OP_IMM},
    {TOK_INSTR_LDU, "ldu", OP_RA | OP_IMM},
    {TOK_INSTR_STM, "stm", OP_RA | OP_RB},
    {TOK_INSTR_LDM, "ldm", OP_RA | OP_RY},

    {TOK_ORG_LABEL, ".label", -1},
    {TOK_ORG_ALIAS, ".alias", -1},
    {TOK_ORG_ORIGIN, ".origin", -1},
    {TOK_ORG_PROG, ".prog", -1},
    {TOK_ORG_DATA, ".data", -1},
    {TOK_ORG_VARIABLE, ".variable", -1},

    {TOK_PINST_RET, "ret", -1},

    {-1, NULL, -1} /* end of table */
};

static struct token regtab[] = {
    {TOK_RF_R0, "r0", 0},
    {TOK_RF_R1, "r1", 1},
    {TOK_RF_R2, "r2", 2},
    {TOK_RF_R3, "r3", 3},
    {TOK_RF_R4, "r4", 4},
    {TOK_RF_R5, "r5", 5},
    {TOK_RF_R6, "r6", 6},
    {TOK_RF_R7, "r7", 7},
    {TOK_RF_R8, "r8", 8},
    {TOK_RF_R9, "r9", 9},
    {TOK_RF_R10, "r10", 10},
    {TOK_RF_R11, "r11", 11},
    {TOK_RF_R12, "r12", 12},
    {TOK_RF_R13, "r13", 13},
    {TOK_RF_R14, "r14", 14},
    {TOK_RF_R15, "r15", 15},

    {-1, NULL, -1} /* end of table */
};

int findRegister(char *tok)
{
    struct token *tt;

    if(tok[0] != 'r') {
        perror("Registers have to start with 'r'!\n");
        exit(EXIT_FAILURE);
    }

    for(tt = regtab; tt->tok != NULL; tt++) {
        if(!strcasecmp(tt->tok, tok))
            return tt->enc;
    }

    /* if nothing retunred yet, the register couldn't be found. ERROR. */
    fprintf(stderr, "Register '%s' not found at %d\n", tok, current_address);
    exit(EXIT_FAILURE);
}

int findVariable(char *v)
{
    int i;

    if(v == NULL)
        return -1;

    for(i = 0; i < vcnt; i++) {
        if(!strcasecmp(v, vtab[i].ident))
            return vtab[i].value;
    }

    return -1;
}


int findLabel(char *l)
{
    int i;

    if(l == NULL)
        return -1;

    for(i = 0; i < lcnt; i++) {
        if(!strcasecmp(l, ltab[i].ident))
            return ltab[i].addr;
    }

    return -1;
}

int identifyToken(char *tok, int *enc)
{
    struct token *tt;

    for(tt = toktab; tt->tok != NULL; tt++) {
        if(!strcasecmp(tt->tok, tok)) {
            if(enc)
                *enc = tt->enc;

            return tt->id;
        }
    }

    if(enc)
        *enc = -1;
    return -1;
}

void parse_variable(void)
{
    int v = 0;
    char *arg, *val;

    if((arg = strtok(NULL, " \t\n")) == NULL) {
        fprintf(stderr, "### ERROR: Failed to parse variable ident\n");
        exit(EXIT_FAILURE);
    }

    if(arg[0] != '%') {
        fprintf(stderr,"### FATAL ERROR : Illegal variable name\"%s\"!\n", arg);
        exit(EXIT_FAILURE);
    }

    if((val = strtok(NULL, " \t\n")) == NULL) {
        fprintf(stderr, "### ERROR: Failed to parse variable value\n");
        exit(EXIT_FAILURE);
    }
    sscanf(val, "%i", &v);

    log_info("## Variable #%d %s at 0x%03d, value= %d\n", 
            vcnt, arg, current_address, v);

    vtab[vcnt].ident = strdup(arg);
    vtab[vcnt].addr = current_address; // this doesn't matter for now
    vtab[vcnt].value = v;
    vcnt++;
}

void parse_label(void)
{
    char *arg;

    arg = strtok(NULL, " \t\n");
    log_info("## Label #%d %s, at 0x%03x\n", lcnt, arg, current_address);

    ltab[lcnt].ident = strdup(arg);
    ltab[lcnt].addr = current_address;
    lcnt++;    
}

void encode_instructions()
{
    int i;
    for(i = 0; i < icnt; i++) {
        /* J format */
        if(itab[i].imm >= 0 && itab[i].regA == -1) {
            itab[i].binValue =
                ((0xf   & itab[i].opc) << 12) |
                ((0xfff & itab[i].imm));
        } else if(itab[i].imm >= 0 && itab[i].regA != -1) { /* I format */
            itab[i].binValue =
                ((0xf   & itab[i].opc) << 12) |
                ((0xf   & itab[i].regA) << 8) |
                ((0xff  & itab[i].imm));
        } else { /* R format */
            itab[i].binValue =
                ((0xf   & itab[i].opc) << 12) |
                ((0xf   & itab[i].regA) << 8) |
                ((0xf   & itab[i].regB) << 4) |
                ((0xf   & itab[i].regY));
        }
    }
}

void to_bitstring(char* buf, int len, int val)
{
    int i;

    if(!buf) return;
    if(len <= 0) return;

    for(i = 0; i < len; i++) {
        buf[(len-1)-i] = (val & (1 << i)) ? '1' : '0';
    }

    buf[len] = (char)0;
}