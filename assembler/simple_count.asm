ld r1 0
ld r1 0
ld r2 1
ld r5 5

.label %loop
jal %addone
stm r5 r1
jal %delayloop


.label %delayloop
    ld r3 0
    ld r4 1
    ldu r10 127
    .label %innerloop
    add r3 r3 r4
    slt r6 r3 r10
    bne r6 %innerloop
    jal %loop


.label %addone
    ld r2 1
    add r1 r1 r2
    jr r15
