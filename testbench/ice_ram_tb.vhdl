-------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 02.08.2020
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_ram_tb is
end ice_ram_tb;

architecture Behavioral of ice_ram_tb is
    signal clk : STD_LOGIC := '0';
    signal wen : STD_LOGIC;
    signal din, dout : STD_LOGIC_VECTOR(15 downto 0);
    signal addr : STD_LOGIC_VECTOR(11 downto 0);
begin

    dut : entity work.ice_ram port map(clk => clk,
                                    addr => addr,
                                    din => din,
                                    wen => wen,
                                    dout => dout);

    clk <= not clk after 5 ns;
    
    proc_stim : process
    begin
        wen <= '1';
        addr <= x"000";
        din <= x"0001";
        wait for 20 ns;

        addr <= x"001";
        din <= x"0002";
        wait for 20 ns;

        addr <= x"002";
        din <= x"0003";
        wait for 20 ns;

        wen <= '0';
        din <= x"0000";
        addr <= x"000";
        wait for 20 ns;

        addr <= x"001";
        wait for 20 ns;

        addr <= x"002";
        wait for 20 ns;

    end process;
end Behavioral;
