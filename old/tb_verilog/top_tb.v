module testbench;
    parameter CLK_PERIOD = 40000; /* 25 MHz = 40000ps */
    reg clk = 0;
    reg clk90 = 0;
    reg reset = 0;
    parameter c_ROWS = 32;
    parameter c_COLS = 16;
    reg [15:0] r_rom_raw[0:c_ROWS-1];

    initial begin
        clk = 1'b0;
        $dumpfile("test.vcd");
        $dumpvars(0, testbench);
        repeat(10) clk = #(CLK_PERIOD/2.0) ~clk;
    end

    initial begin
        clk90 = 1'b0;
        #10000; // to make it 90 degrees out of phase
        repeat (10) clk90 = #(CLK_PERIOD/2.0) ~clk90;
    end

    /*initial begin
        r_rom_raw[0] = 16'b0000000000000000;
        r_rom_raw[1] = 16'b1100000000000101;
        r_rom_raw[2] = 16'b1100000100001001;
        r_rom_raw[3] = 16'b1100001100101010;
        r_rom_raw[4] = 16'b0100000000010010;
        r_rom_raw[5] = 16'b1110001100100000;
        r_rom_raw[6] = 16'b0101001000010010;
        r_rom_raw[7] = 16'b1110001100100000;
        r_rom_raw[8] = 16'b0110000000010010;
        r_rom_raw[9] = 16'b1110001100100000;
        r_rom_raw[10] = 16'b0111000000010010;
        r_rom_raw[11] = 16'b1110001100100000;
        r_rom_raw[12] = 16'b1000000000010010;
        r_rom_raw[13] = 16'b1110001100100000;
        r_rom_raw[14] = 16'b1001000000010010;
        r_rom_raw[15] = 16'b1110001100100000;
        r_rom_raw[16] = 16'b1010000000010010;
        r_rom_raw[17] = 16'b1110001100100000;
        r_rom_raw[18] = 16'b1011000000010010;
        r_rom_raw[19] = 16'b1110001100100000;
        r_rom_raw[20] = 16'b1011000100000010;
        r_rom_raw[21] = 16'b1110001100100000;
        r_rom_raw[22] = 16'b0000000000000000;
        r_rom_raw[23] = 16'b0000000000000000;
        r_rom_raw[24] = 16'b0000000000000000;
        r_rom_raw[25] = 16'b0000000000000000;
        r_rom_raw[26] = 16'b0000000000000000;
        r_rom_raw[27] = 16'b0000000000000000;
        r_rom_raw[28] = 16'b0000000000000000;
        r_rom_raw[29] = 16'b0000000000000000;
        r_rom_raw[30] = 16'b0000000000000000;
        r_rom_raw[31] = 16'b0000000000000000;
    end*/

    //assign o_dout = r_rom_raw[i_addr[4:0]];
    wire [11:0] inst_addr;
    wire read_strobe, write_strobe;
    wire [15:0] mem_add;
    wire [15:0] mem_in;
    wire [15:0] mem_out;
    assign mem_in = 0;

    ice_proc dut(
    .clk(clk),
    .clk90(clk90),
    .reset(reset),
    .instr_addr(inst_addr),
    /*.instruction(r_rom_raw[inst_addr[4:0]]),*/
    .instruction(16'b1100000000000101),
    .mem_addr(mem_add),
    .mem_in(mem_in),
    .mem_out(mem_out),
    .mem_rd(read_strobe),
    .mem_wr(write_strobe)
    );
endmodule

