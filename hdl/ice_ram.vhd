-------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 02.08.2020
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity ice_ram is
    port(clk : in STD_LOGIC;
        addr : in STD_LOGIC_VECTOR(11 downto 0);
        din : in STD_LOGIC_VECTOR(15 downto 0);
        wen : in STD_LOGIC;
        dout : out STD_LOGIC_VECTOR(15 downto 0));
end ice_ram;


architecture Behavioral of ice_ram is
    type RAM_ARRAY is array (0 to 127) of std_logic_vector (15 downto 0);

    signal ram_data : RAM_ARRAY;
begin 
    process(clk)
    begin
        if clk'event and clk = '1' then
            if wen = '1' then
                ram_data(to_integer(unsigned(addr(6 downto 0)))) <= din;
            end if;
        end if;
    end process;

    dout <= ram_data(to_integer(unsigned(addr(6 downto 0))));

end Behavioral;
