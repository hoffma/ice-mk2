module ice_rf(
    input clk,
    input reset,
    input [3:0] rp0_addr,
    input [3:0] rp1_addr,
    input [3:0] wp_addr,
    output [15:0] rp0_data,
    output [15:0] rp1_data,
    input [15:0] wp_data,
    input wp_en
);

wire i_clk;
assign i_clk = clk;

reg [15:0] o_rp0_data;
reg [15:0] o_rp1_data;

reg [15:0] s_sel;
reg [15:0] s_rf[16];

ice_rfdecode dec(
    .addr(wp_addr),
    .en(wp_en),
    .sel(s_sel)
);

/*always @(*)
begin
    if(wp_en == 1) begin
        s_rf[wp_addr] = wp_data;
    end
end*/

assign o_rp0_data = s_rf[rp0_addr];
assign o_rp1_data = s_rf[rp1_addr];


genvar i;
generate
    for(i = 0; i < 16; i = i+1) begin
        ice_reg regX(
            .clk(i_clk),
            .reset(reset),
            .en(s_sel[i]),
            .d(wp_data),
            .q(s_rf[i])
        );
    end
endgenerate

assign rp0_data = o_rp0_data;
assign rp1_data = o_rp1_data;

endmodule
