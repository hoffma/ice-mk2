module ice_alu(
    input [15:0] a,
    input [15:0] b,
    input [3:0] opc,
    output [15:0] y,
    output c
);

/* REGISTER-REGISTER */
localparam ADD = 4'b0100;
localparam SUB = 4'b0101;
localparam AND = 4'b0110;
localparam OR = 4'b0111;
localparam XOR = 4'b1000;
localparam LSL = 4'b1001;
localparam LSR = 4'b1010;
localparam SLT = 4'b1011;

reg [16:0] o_y;
assign y = o_y[15:0];
assign c = o_y[16];

always @(*)
begin
    case(opc)
        ADD: o_y = $signed(a) + $signed(b);
        SUB: o_y = $signed(a) - $signed(b);
        AND: o_y[15:0] = a & b;
        OR: o_y[15:0] = a | b;
        XOR: o_y[15:0] = a ^ b;
        LSL: o_y[15:0] = a << b;
        LSR: o_y[15:0] = a >> b;
        SLT: o_y = (a < b) ? 1 : 0;
        default: o_y = 17'b0;
    endcase
end

endmodule
