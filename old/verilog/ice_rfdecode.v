module ice_rfdecode(
    input [3:0] addr,
    input en,
    output [15:0] sel
);

reg [15:0] o_sel;
assign sel = o_sel;

always @*
begin
    o_sel = 0;
    if(en == 1) begin
        o_sel[addr] = 1;
    end
end

endmodule
