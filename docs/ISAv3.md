# Instruction Set Architecture ICE-Processor

## Formats

The instructions set contains 3 formats. J-, I- and R-Format. J is for Jump 
instructions containing only an immediate value, I is immediate based 
instructions and the R-format is for all register-register based instructions.
The register based instructions have a 2 address format. All three formats 
are listed below:

J: | opc 15 .. 11 | immediate 10 .. 0 |
I: | opc 15 .. 11 | ra 10 .. 8 | immediate 7 .. 0 |
R: | opc 15 .. 11 | ra 10 .. 8 | rb 7 .. 5 | ry 4 .. 2 | * 1..0 |

16 bit
8 registers
16 bit word width
16 bit instruction width


## Instructions
0x00 noop

### Jump Instructions
0x01 j
0x02 jal
0x03 jr
0x04 brnz
0x05 brz

### Memory Instructions 
0x1c ld
0x1d ldu
0x1e stm
0x1f ldm

### Register Register Instructions
0x06 add
0x07 sub
0x08 mod
0x09 and
0x0a or
0x0b xor
0x0c lsl
0x0d lsr
0x0e not
0x0f slt
0x13 mul
0x14 div

### Register Immediate Instructions
0x10 addi
0x11 subi
0x12 andi


