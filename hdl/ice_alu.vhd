----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ice_alu is
    generic (width : positive := 16);
    port(a : in STD_LOGIC_VECTOR(width-1 downto 0);
        b : in STD_LOGIC_VECTOR(width-1 downto 0);
        imm : in STD_LOGIC_VECTOR(7 downto 0);
        y : out STD_LOGIC_VECTOR(width-1 downto 0);
        c : out STD_LOGIC;
        opc : in STD_LOGIC_VECTOR(4 downto 0));
end ice_alu;

architecture Behavioral of ice_alu is
    -- signal o_y : STD_LOGIC_VECTOR(width downto 0);
    signal o_y: std_logic_vector(width downto 0)   := (others => '0');
    signal s_lt : std_logic;
    signal shift_o, tmp1, tmp2, tmp3 : std_logic_vector(width-1 downto 0);
    signal shamt : std_logic_vector(3 downto 0);
    signal imm_in : std_logic_vector(width-1 downto 0);
    signal mul_o : std_logic_vector((width*2)-1 downto 0);
begin

    s_lt <= '1' when (signed(a) < signed(b)) else '0';
    imm_in <= (8 downto 0 => imm(7)) & imm(6 downto 0);
    -- vector can have double the width after multiplication
    mul_o <= std_logic_vector(signed(a) * signed(b)); 

    with opc select o_y(15 downto 0) <=
    std_logic_vector(signed(a) + signed(b)) when "00110",
    std_logic_vector(signed(a) - signed(b)) when "00111",
    std_logic_vector((signed(a) mod signed(b))) when "01000",
    (a and b) when "01001",
    (a or b) when "01010",
    (a xor b) when "01011",
    shift_o when "01100", -- 1001 LSL
    shift_o when "01101", -- 1010 LSR
    (not a) when "01110",
    ("000000000000000" & s_lt) when "01111",
    std_logic_vector(signed(a) + signed(imm)) when "10000",
    std_logic_vector(signed(a) - signed(imm)) when "10001",
    (a and imm_in) when "10010",
    mul_o(15 downto 0) when "10011",
    std_logic_vector(signed(a) / signed(b)) when "10100",
    (others => '0') when others;

    shamt <= b(3 downto 0);

    tmp1 <= a(width-2 downto 0) & '0' when opc&shamt(0) = "011001" else -- left
            '0' & a(width-1 downto 1) when opc&shamt(0) = "011011" else -- right
            a; -- no shift

    tmp2 <= tmp1(width-3 downto 0) & "00" when opc&shamt(1) = "011001" else
            "00" & tmp1(width-1 downto 2) when opc&shamt(1) = "011011" else
            tmp1;

    tmp3 <= tmp2(width-5 downto 0) & "0000" when opc&shamt(2) = "011001" else
            "0000" & tmp2(width-1 downto 4) when opc&shamt(2) = "011011" else
            tmp2;
    
    shift_o <= tmp3(width-9 downto 0) & "00000000" when opc&shamt(3) = "011001" else
               "00000000" & tmp3(width-1 downto 8) when opc&shamt(3) = "011011" else
               tmp3;

    y <= o_y(15 downto 0);
    c <= o_y(16);

end Behavioral;
