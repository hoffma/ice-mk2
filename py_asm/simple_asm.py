from parse import ASMSemantics

INSTRUCTIONS = {
    "nop" : 0x00,
    "j" : 0x01,
    "jal" : 0x02,
    "jr" : 0x03,
    "brnz" : 0x04,
    "brz" : 0x05,
    
    "add" : 0x06,
    "sub" : 0x07,
    "mod" : 0x08,
    "and" : 0x09,
    "or" : 0x0a,
    "xor" : 0x0b,
    "lsl" : 0x0c,
    "lsr" : 0x0d,
    "not" : 0x0e,
    "slt" : 0x0f,

    "addi" : 0x10,
    "subi" : 0x11,
    "andi" : 0x12,

    "ld" : 0x1c,
    "ldu" : 0x1d,
    "stm" : 0x1e,
    "ldm" : 0x1f
}

inst_tab = []
lbl_tab = {}
const_tab = {}

class Instruction():
    def __init__(self):
        self.opc = 0x00
        self.type = 0
        self.reg0 = 0
        self.reg1 = 0
        self.reg2 = 0
        self.imm = 0
        self.label= ""
        self.resolveLabel = False
        self.const = ""
        self.resolveConst = False
        self.addr = 0
        self.value = 0

    def __repr__(self):
        s = "0x%04x: opc:0x%02x, " % (self.addr, self.opc)
        if self.type == 1:
            s += "imm: 0x%03x" % self.imm
        elif self.type == 2:
            s += "reg: %s" % self.reg0
        elif self.type == 3:
            s += "reg: %s, imm: 0x%03x" % (self.reg0, self.imm)
        elif self.type == 4:
            if self.opc == 0x1e:
                s += "reg0: %s, reg2: %s" % (self.reg0, self.reg1)
            else:
                s += "reg0: %s, reg1: %s" % (self.reg0, self.reg2)
        elif self.type == 5:
            s += "reg0: %s, reg1: %s, reg2: %s" % (self.reg0, self.reg1, self.reg2)

        s += "; %s" % self.value

        return s


    def genCode(self):
        return 0


class SemanticThing(ASMSemantics):
    def __init__(self):
        self.currentAddress = 0

    def misc_instruction(self, ast):  
        i = Instruction()
        i.type = 0
        i.opc = INSTRUCTIONS[ast.opc]
        i.addr = self.currentAddress

        self.currentAddress += 1
        inst_tab.append(i)
        return i

    def imm_instruction(self, ast):  
        i = Instruction()
        i.type = 1
        i.opc = INSTRUCTIONS[ast.opc]
        i.addr = self.currentAddress

        if isinstance(ast.imm, int):
            i.imm = ast.imm
        else:
            i.label = ast.imm[1:]
            i.resolveLabel = True

        inst_tab.append(i)
        self.currentAddress += 1
        return i
    
    def reg_instruction(self, ast):  
        i = Instruction()
        i.type = 2
        i.opc = INSTRUCTIONS[ast.opc]
        i.addr = self.currentAddress
        i.reg0 = ast.reg0
        i.imm = 0

        inst_tab.append(i)
        self.currentAddress += 1
        return i

    def reg_imm_instruction(self, ast):  
        i = Instruction()
        i.type = 3
        i.opc = INSTRUCTIONS[ast.opc]
        i.addr = self.currentAddress
        if isinstance(ast.imm, int):
            i.imm = ast.imm
        else:
            i.const = ast.imm
            i.resolveConst = True

        i.reg0 = ast.reg0

        inst_tab.append(i)
        self.currentAddress += 1
        return i

    def two_reg_instruction(self, ast):  
        i = Instruction()
        i.type = 4
        i.opc = INSTRUCTIONS[ast.opc]
        i.addr = self.currentAddress

        if i.opc == 0x1e:
            i.reg0 = ast.reg0
            i.reg1 = ast.reg1
        else:
            i.reg0 = ast.reg0
            i.reg2 = ast.reg1 

        inst_tab.append(i)
        self.currentAddress += 1
        return i

    def three_reg_instruction(self, ast):  
        i = Instruction()
        i.type = 5
        i.opc = INSTRUCTIONS[ast.opc]
        i.addr = self.currentAddress

        i.reg0 = ast.reg0
        i.reg1 = ast.reg1
        i.reg2 = ast.reg2

        self.currentAddress += 1
        return i

    def register(self, ast):
        return int(ast[1])

    def number(self, ast):  
        return int(ast, 0)

    def label(self, ast):
        if ast.name not in lbl_tab:
            lbl_tab[ast.name] = self.currentAddress
        else:
            raise NameError('Label "%s" already exists at address: 0x%x' % (ast.name, lbl_tab[ast.name]))
        return ast

    def constant(self, ast):
        if ast.name not in lbl_tab:
            const_tab[ast.name] = ast.value
        else:
            raise NameError('Constant "%s" already exists with value: 0x%x' % (ast.name, const_tab[ast.name]))
        return ast


if __name__ == '__main__':
    import pprint
    import json
    import sys
    import tatsu
    from tatsu import parse
    from tatsu.util import asjson
    from parse import ASMParser

    grammar = open('grammar.ebnf', 'r').read()

    if len(sys.argv) != 2:
        raise ValueError('Wrong number of arguments')

    file = open(sys.argv[1], 'r')
    contents = file.read()

    parser = ASMParser()

    # FIRST PASS
    ast = parser.parse(contents, semantics=SemanticThing())
    # RESOLVE LABELS AND GENERATE KODE
    for a in ast:
        if isinstance(a, Instruction):
            if a.resolveLabel:
                if a.label not in lbl_tab:
                    raise NameError('Error resolving label "%s"!' % a.label)
                a.imm = lbl_tab[a.label]
            if a.resolveConst:
                if a.const not in const_tab:
                    raise NameError('Error resolving const "%s"!' % a.const)
                a.imm = const_tab[a.const]


            if a.type == 0:
                a.value = "{0:05b}{1:011b}".format(a.opc, 0)
            elif a.type == 1:
                a.value = "{0:05b}{1:011b}".format(a.opc, a.imm&0x3ff)
            elif a.type == 2:
                a.value = "{0:05b}{1:03b}{2:08b}".format(a.opc, a.reg0, 0)
            elif a.type == 3:
                a.value = "{0:05b}{1:03b}{2:08b}".format(a.opc, a.reg0, a.imm&0xff)
            elif a.type == 4 or a.type == 5:
                a.value = "{0:05b}{1:03b}{2:03b}{3:03b}{4:02b}".format(a.opc, a.reg0, a.reg1, a.reg2, 0)
        else:
            print(type(a))

    print('# PPRINT')
    pprint.pprint(ast, indent=2, width=20)
    print()

    print(lbl_tab)

    # ROM_SIZE = 131072
    ROM_SIZE = 4096
    count = 0
    basename = sys.argv[1].split('.')[0]
    outname = basename + ".vhdl"
    f = open(outname,"w+")
    f.write("library IEEE;\n")
    f.write("use IEEE.STD_LOGIC_1164.all;\n")
    f.write("\n")
    f.write("package imem is\n");
    f.write("type mem is array ( 0 to %d) of std_logic_vector(15 downto 0);\n" % int(ROM_SIZE-1));
    f.write("\n");
    f.write("constant imem_rom : mem := (\n");

    for a in ast:
        if isinstance(a, Instruction):
            f.write("%d => \"%s\"" % (count, a.value))
            if count < ROM_SIZE-1:
                f.write(",\n")
            else:
                f.write("\n")
            count +=1

    while count < ROM_SIZE:
        f.write("%d => \"%016d\"" % (count, 0))
        if count < ROM_SIZE-1:
            f.write(",\n")
        else:
            f.write("\n")
        count +=1

    f.write(");\n");
    f.write("end imem;\n");


    f.close()

    # print('# JSON')
    # print(json.dumps(asjson(ast), indent=2))
    # print()
