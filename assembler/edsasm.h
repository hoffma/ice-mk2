

/* token id definition */

#define TOK_INSTR_ADD         1
#define TOK_INSTR_SUB         2
#define TOK_INSTR_INC         3
#define TOK_INSTR_DEC         4

#define TOK_INSTR_LD         10
#define TOK_INSTR_LDM        11
#define TOK_INSTR_STM        12

#define TOK_INSTR_BRA        20
#define TOK_INSTR_BRT        21
#define TOK_INSTR_BRF        22

#define TOK_INSTR_AND        30
#define TOK_INSTR_OR         30
#define TOK_INSTR_NAND       32
#define TOK_INSTR_NOR        33
#define TOK_INSTR_XOR        34
#define TOK_INSTR_XNOR       35
#define TOK_INSTR_NOT        36

#define TOK_INSTR_SL         40
#define TOK_INSTR_LSR        41
#define TOK_INSTR_ASR        42
#define TOK_INSTR_ROL        43
#define TOK_INSTR_ROR        44

#define TOK_INSTR_CMPLT      51
#define TOK_INSTR_CMPGT      52
#define TOK_INSTR_CMPLEQ     53
#define TOK_INSTR_CMPGEQ     54
#define TOK_INSTR_CMPEQ      55
#define TOK_INSTR_CMPNEQ     56
#define TOK_INSTR_CMPZ       57
#define TOK_INSTR_CMPNZ      58

#define TOK_ORG_ORIGIN      100
#define TOK_ORG_LABEL       101
#define TOK_ORG_IMM         102
#define TOK_ORG_PROG        103
#define TOK_ORG_DATA        104
#define TOK_ORG_STRING      105
#define TOK_ORG_VARIABLE    106

#define TOK_RF_R0           200
#define TOK_RF_R1           201
#define TOK_RF_R2           202
#define TOK_RF_R3           203
#define TOK_RF_R4           204
#define TOK_RF_R5           205
#define TOK_RF_R6           206
#define TOK_RF_R7           207
#define TOK_RF_R8           208
#define TOK_RF_R9           209
#define TOK_RF_R10          210
#define TOK_RF_R11          211
#define TOK_RF_R12          212
#define TOK_RF_R13          213
#define TOK_RF_R14          214
#define TOK_RF_R15          215


/* operand encoding */

#define OP_NULL             0x0
#define OP_IMM              0x1
#define OP_SRC0             0x2
#define OP_SRC1             0x4
#define OP_DST              0x8



/* instruction group code  */

#define GRP_BRA     0x0   
#define GRP_ARITH   0x1   
#define GRP_LOGIC   0x2   
#define GRP_SHIFT   0x3   
#define GRP_CMP     0x4   
#define GRP_MEM     0x5   


/* per group operation codes */

#define BRA_BRA     0x0
#define BRA_BRT     0x1
#define BRA_BRF     0x2

#define ARITH_ADD   0x0
#define ARITH_SUB   0x1
#define ARITH_INC   0x2
#define ARITH_DEC   0x3

#define LOGIC_AND   0x0
#define LOGIC_OR    0x1
#define LOGIC_NAND  0x2
#define LOGIC_NOR   0x3
#define LOGIC_XOR   0x4
#define LOGIC_XNOR  0x5
#define LOGIC_NOT   0x6

#define SHIFT_SL    0x0
#define SHIFT_LSR   0x1
#define SHIFT_ASR   0x2
#define SHIFT_ROL   0x3
#define SHIFT_ROR   0x4

#define CMP_LT      0x0
#define CMP_GT      0x1
#define CMP_LEQ     0x2
#define CMP_GEQ     0x3
#define CMP_EQ      0x4
#define CMP_NEQ     0x5
#define CMP_Z       0x6
#define CMP_NZ      0x7
#define CMP_TST     0x8

#define MEM_LDM     0x0
#define MEM_STM     0x1
#define MEM_LDIO    0x2
#define MEM_STIO    0x3


struct token {
  int id;
  char *tok;
  int param;
  int p1;
  int p2;
};

struct variable {
  char *ident;
  int addr;
  int value;
};

struct label {
  char *ident;
  int addr;
};


struct instr {
  int   addr;       // instruction address
  int   imm;        // immediate address e.g. for jump instruction
  int   imm_flag;   // indicate that immediate value is valid
  int   grp;        // instruction group code
  int   opc;        // operation code
  int   dst;        // destination oeprand address (register file)
  int   src0;       // source #0 operand address
  int   src1;       // source #1 operand address 
  char *var;        // variable identifier if present in source
  char *label;      // label identifier if present in source
  int   value;      // encoded instruction value
};


extern struct variable vtab[4096];
extern struct label ltab[4096];
extern struct instr itab[4096];

extern int vcnt;
extern int lcnt;
extern int icnt;

extern int findRegister (char *tok);
extern int findVariable (char *tok);
extern int findLabel (char *tok);

extern int identifyToken (char *tok, int *p, int *p1, int *p2);

extern void tobitstring (char *buf, int len, int value);

void instr_encode (void);
