library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.STD_LOGIC_UNSIGNED.all;

entity bcd is
    port(binary     : in std_logic_vector(15 downto 0);
        thousands   : out std_logic_vector(3 downto 0);
        hundreds    : out std_logic_vector(3 downto 0);
        tens        : out std_logic_vector(3 downto 0);
        ones        : out std_logic_vector(3 downto 0));
end bcd;

-- https://www.youtube.com/watch?v=VKKGyOc4zRA
-- starting 9:00
architecture Behavioral of bcd is
begin
    process(binary)
        variable z : STD_LOGIC_VECTOR(31 downto 0);
    begin
        z := (others => '0');

        z(18 downto 3) := binary;

        for i in 0 to 12 loop
            if z(19 downto 16) > 4 then
                z(19 downto 16) := z(19 downto 16) + 3;
            end if;
            if z(23 downto 20) > 4 then
                z(23 downto 20) := z(23 downto 20) + 3;
            end if;
            if z(27 downto 24) > 4 then
                z(27 downto 24) := z(27 downto 24) + 3;
            end if;
            if z(31 downto 28) > 4 then
                z(31 downto 28) := z(31 downto 28) + 3;
            end if;
            z(31 downto 1) := z(30 downto 0);
        end loop;

        thousands <= z(31 downto 28);
        hundreds <= z(27 downto 24);
        tens <= z(23 downto 20);
        ones <= z(19 downto 16);
    end process;

end Behavioral;