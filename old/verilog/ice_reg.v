module ice_reg(
    input clk,
    input reset,
    input [15:0] d,
    output [15:0] q,
    input en
);


wire i_clk;
assign i_clk = clk;

reg [15:0] o_q;
assign q = o_q;

always @(posedge i_clk)
begin
    if(en == 1) begin
        o_q <= d;
    end
    if(reset == 1) begin
        o_q <= 0;
    end
end

endmodule
