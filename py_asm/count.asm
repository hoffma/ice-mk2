

.const $zero 0x0
.const $ledAddr 0x05
.const $saveAddr 0xff
.const $delayCount 0x03

ld r1 $zero
ld r5 $ledAddr
ld r6 $saveAddr

.label loop
    addi r1 1
    stm r5 r1
    stm r6 r1 ; store current count to ram
    jal %delay  ; jump delay
    ldm r6 r1 ; load count back from ram
    j %loop

.label delay
    ld r1 $zero
    ldu r3 $delayCount
.label delayloop
    addi r1 1
    slt r4 r1 r3
    brnz r4 -2
    jr r7

