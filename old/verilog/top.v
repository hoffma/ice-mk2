
`ifdef VERILATOR
module top(
    /* verilator lint_off UNUSED */
    input clk,
    input clk0,
    input clk90,
    input reset,
    output [11:0] instr_addr,
    input [15:0] instruction,
    output [15:0] mem_addr,
    input [15:0] mem_in,
    output [15:0] mem_out,
    output mem_rd,
    output mem_wr
); 

wire i_clk, i_clk90;
assign i_clk = clk0;
assign i_clk90 = clk90;

`else
module top(
    /* verilator lint_off UNUSED */
    input clk_25mhz,
    input [6:0] btn,
    input [3:0] sw,
    output [7:0] led,
    output wifi_gpio0
);

assign wifi_gpio0 = 1;
`endif

ice_proc inst_proc(
    .clk(i_clk),
    .clk90(i_clk90),
    .reset(reset),
    .instr_addr(instr_addr),
    .instruction(instruction),
    .mem_addr(mem_addr),
    .mem_in(mem_in),
    .mem_out(mem_out),
    .mem_rd(mem_rd),
    .mem_wr(mem_wr)
);

endmodule
