------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 02.08.2020
------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ice_rf is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           rp0_addr : in  STD_LOGIC_VECTOR (2 downto 0);
           rp0_data : out  STD_LOGIC_VECTOR (15 downto 0);
           rp1_addr : in  STD_LOGIC_VECTOR (2 downto 0);
           rp1_data : out  STD_LOGIC_VECTOR (15 downto 0);
           wp_addr : in  STD_LOGIC_VECTOR (2 downto 0);
           wp_data : in  STD_LOGIC_VECTOR (15 downto 0);
           wp_en : in  STD_LOGIC);
end ice_rf;

architecture Behavioral of ice_rf is
	type register_array is array(15 downto 0) of
						STD_LOGIC_VECTOR(15 downto 0);
	
	signal s_sel : STD_LOGIC_VECTOR(7 downto 0);
	signal s_rf : register_array;	
	signal s_en : STD_LOGIC;
begin
    -- reg0 is always zero
    reg0 : entity work.ice_reg port map(clk, reset, (others => '0'), s_rf(0), s_sel(0));
	preg: for i in 1 to 7 generate
        reg : entity work.ice_reg port map(clk, reset, wp_data, s_rf(i), s_sel(i));
	end generate preg;
	
    dec : entity work.ice_rfdecode port map(wp_addr, wp_en, s_sel);

    rp1_data <= s_rf(to_integer(unsigned(rp1_addr))); -- when rp1_addr /= "X" else (others => '0');
    rp0_data <= s_rf(to_integer(unsigned(rp0_addr))); -- when rp0_addr /= "X" else (others => '0');
end Behavioral;

