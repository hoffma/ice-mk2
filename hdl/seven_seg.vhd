library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity seven_seg is
    port(clk    : in STD_LOGIC;
        bcd     : in STD_LOGIC_VECTOR(15 downto 0);
        digit   : out STD_LOGIC_VECTOR(3 downto 0);
        segment : out STD_LOGIC_VECTOR(6 downto 0)
    );
end seven_seg;

architecture Behavioral of seven_seg is
    signal x : std_logic_vector(3 downto 0);
    signal state : std_logic_vector(1 downto 0);
    signal state_counter : std_logic_vector(15 downto 0) := (others => '0');
begin
    process(clk)
    begin
        if clk'event and clk = '1' then
            state_counter <= state_counter + 1;
        
            case state is
                when "00" => 
                    digit <= "1000";
                    x <= bcd(3 downto 0);
                    if state_counter >= 24999 then
                        state <= "01";
                        state_counter <= (others => '0');
                    end if;
                when "01" => 
                    digit <= "0100";
                    x <= bcd(7 downto 4);
                    if state_counter >= 24999 then
                        state <= "10";
                        state_counter <= (others => '0');
                    end if;
                when "10" =>
                    digit <= "0010";
                    x <= bcd(11 downto 8);
                    if state_counter >= 24999 then
                        state <= "11";
                        state_counter <= (others => '0');
                    end if;
                when "11" =>
                    digit <= "0001";
                    x <= bcd(15 downto 12);
                    if state_counter >= 24999 then
                        state <= "00";
                        state_counter <= (others => '0');
                    end if;
                when others =>
                    state <= "00";
                    state_counter <= (others => '0');
                end case;
        end if;
    end process;

    segment(0) <= (x(2) and x(1) and not x(0)) or
                (x(2) and not x(1) and not x(0)) or
                (not x(3) and not x(2) and not x(1) and x(0));
    
    segment(1) <= (x(2) and x(1) and not x(0)) or
                (x(2) and not x(1) and x(0));

    segment(2) <= (not x(2) and x(1) and not x(0));

    segment(3) <= (x(2) and x(1) and x(0)) or
                (x(2) and not x(1) and not x(0)) or
                (not x(2) and not x(1) and x(0));

    segment(4) <= (x(2) and not x(1) and not x(0)) or x(0);

    segment(5) <= (x(2) and x(1) and x(0)) or
                (not x(2) and x(1)) or
                (not x(3) and not x(2) and not x(1) and x(0));

    segment(6) <= (x(2) and x(1) and x(0)) or 
                (not x(3) and not x(2) and not x(1));

end Behavioral;