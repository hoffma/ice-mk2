PHONY: all
.DELETE_ON_ERROR:
TOPMOD  := top
TOPFILE := hdl/$(TOPMOD).vhdl
ADDFILES := hdl/ecp5pll.vhdl hdl/bcd.vhdl hdl/seven_seg.vhdl hdl/ice_rom.vhdl hdl/ice_alu.vhdl hdl/ice_pc.vhdl hdl/ice_reg.vhdl hdl/ice_rfdecode.vhdl hdl/ice_rf.vhdl hdl/ice_ram.vhdl hdl/ice_proc.vhdl hdl/ice_soc.vhdl
VHDFILES := $(ADDFILES) $(TOPFILE)
SIMMOD := $(TOPMOD)_tb
TBFILE := testbench/$(SIMMOD).vhdl
SIMFILE := $(TOPMOD)_sim.ghk 

all: $(SIMFILE)

#PATH := /home/user/ulx3s_workspace/install/bin:$(PATH)

## 
.PHONY: clean
clean:
	rm -rf $(BINFILE)
	rm -rf $(TOPMOD).json ulx3s_out.config ulx3s.bit *.cf
	rm -rf $(SIMFILE)
	rm -rf *.log

$(SIMFILE): $(ADDFILES) $(TBFILE)
	ghdl -a -fsynopsys $^

ulx3s.bit: ulx3s_out.config
	ecppack -v --compress --freq 62.0 ulx3s_out.config ulx3s.bit

ulx3s_out.config: $(TOPMOD).json
	nextpnr-ecp5 -l "pnr.log" -v --45k --json $(TOPMOD).json --package CABGA381 --lpf ulx3s_v20.lpf --textcfg ulx3s_out.config 

$(TOPMOD).json: 
	#yosys -l "sys.log" -p 'ghdl -fsynopsys $(VHDFILES) -e $(TOPMOD); hierarchy -top top; synth_ecp5 -json $(TOPMOD).json'
	yosys -l "sys.log" synth.ys

sim:
	ghdl -a $(VHDFILES) $(TBFILE)
	ghdl -r $(SIMMOD) --vcd=$(SIMFILE)

prog: ulx3s.bit
	ujprog $^
