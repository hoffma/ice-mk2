#include <verilatedos.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include "verilated.h"
#include "Vtop.h"
#include "testb.h"

int	main(int argc, char **argv) {
	Verilated::commandArgs(argc, argv);
	TESTB<Vtop>	*tb
		= new TESTB<Vtop>;
	tb->opentrace("top.vcd");

	tb->m_core->a = 5;
	tb->m_core->b = 9;
    tb->m_core->imm = 0;
    tb->m_core->imm_op = 0;

    tb->m_core->opc = 0b00001;
    tb->tick();
	tb->m_core->b = -9;
    tb->m_core->opc = 0b00010;
    tb->tick();

	tb->m_core->a = 2;
	tb->m_core->b = 3;
    tb->m_core->opc = 0b00100;
    tb->tick();
    tb->m_core->opc = 0b00101;
    tb->tick();
    tb->m_core->opc = 0b00110;
    tb->tick();

	tb->m_core->a = 1;
	tb->m_core->b = 3;
    tb->m_core->opc = 0b01000;
    tb->tick();
	tb->m_core->a = 16;
    tb->m_core->opc = 0b01001;
    tb->tick();
	tb->m_core->a = -16;
    tb->m_core->opc = 0b01010;
    tb->tick();

	tb->m_core->a = 1;
	tb->m_core->b = 9;
    tb->m_core->opc = 0b10000;
    tb->tick();
    tb->m_core->opc = 0b10001;
    tb->tick();
	tb->m_core->b = 1;
    tb->m_core->opc = 0b10001;
    tb->tick();
    
    
	printf("\n\nSimulation complete\n");
}
