ld r0 0
ld r0 0
ld r1 1
ld r2 16
ld r3 0
ld r5 5
ld r6 10

.label %up
add r0 r0 r1
stm r5 r0
stm r6 r0
slt r3 r0 r2
bne r3 %up

.label %down
sub r0 r0 r1
stm r5 r0
stm r6 r0
slt r3 r0 r1
beq r3 %down
