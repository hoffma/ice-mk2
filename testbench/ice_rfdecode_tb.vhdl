
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_rfdecode_tb is
end ice_rfdecode_tb;

architecture Behavioral of ice_rfdecode_tb is
	
	signal s_addr 	: STD_LOGIC_VECTOR(2 downto 0);
	signal s_en		: STD_LOGIC;
	signal s_sel	: STD_LOGIC_VECTOR(7 downto 0);

begin
	dut : entity work.ice_rfdecode port map(addr => s_addr,
								en => s_en,
								sel => s_sel);
	
	s_addr <= "000", "001" after 100 ns, "010" after 200 ns, "011" after 300 ns, "100" after 400 ns, "101" after 500 ns, "110" after 600 ns, "111" after 700 ns, "000" after 800 ns;
	s_en <= '0', '1' after 50 ns;

end Behavioral;

