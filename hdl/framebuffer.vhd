-- vga with 640x480@60Hz
-- http://www.tinyvga.com/vga-timing/640x480@60Hz

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity framebuffer is
    generic (
        bits_x : integer := 10; -- 800px total
        bits_y : integer := 10; -- 525px total
        pixels_x : integer := 640;
        pixels_y : integer := 480;
        pixel_depth : integer := 8
    );
    port (
        clk : in std_logic; -- should be 25MHz input clk
        -- physical connections
        vsync       : out std_logic;
        hsync       : out std_logic;
        vga_r       : out std_logic;
        vga_g       : out std_logic;
        vga_b       : out std_logic;

        beam_x      : out std_logic_vector(bits_x-1 downto 0);
        beam_y      : out std_logic_vector(bits_y-1 downto 0);
        pixel_in    : in std_logic_vector(pixel_depth-1 downto 0)

    );
end framebuffer;

architecture Behavioral of framebuffer is
    constant PX_WIDTH : integer := 640;
    constant PX_HEIGHT : integer := 480;
    constant V_SYNC_PULSE : integer := 2;
    constant V_BACK_PORCH : integer := 33;
    constant V_FRONT_PORCH : integer := 10;
    constant H_SYNC_PULSE : integer := 96;
    constant H_BACK_PORCH : integer := 48;
    constant H_FRONT_PORCH : integer := 16;

    signal i_clk : std_logic;
    signal count_x : unsigned(9 downto 0);
    signal count_y : unsigned(9 downto 0);
    signal pos_x : unsigned(bits_x-1 downto 0) := (others => '0');
    signal pos_y : unsigned(bits_y-1 downto 0) := (others => '0');

    signal s_r, s_g, s_b, s_hsync, s_vsync : std_logic;
begin
    i_clk <= clk;
    vga_r <= s_r;
    vga_g <= s_g;
    vga_b <= s_b;
    hsync <= s_hsync;
    vsync <= s_vsync;
    beam_x <= std_logic_vector(pos_x);
    beam_y <= std_logic_vector(pos_y);

    process(i_clk)
    variable tmp : unsigned(11 downto 0);
    begin
        if rising_edge(i_clk) then
            s_r <= '0';
            s_g <= '0';
            s_b <= '0';
            s_vsync <= '0';
            s_hsync <= '0';
            count_x <= count_x + 1; -- x count always runs

            if count_x < H_SYNC_PULSE-1 then
                s_hsync <= '1';
            elsif count_x >= (H_SYNC_PULSE + H_BACK_PORCH - 1) 
                and count_x < (800-H_FRONT_PORCH-1) 
                and count_y >= (V_SYNC_PULSE + V_BACK_PORCH - 1) then
                    s_r <= pixel_in(7);
                    s_g <= pixel_in(4);
                    s_b <= pixel_in(1);
                    pos_x <= pos_x + 1;
            elsif count_x >= (800-1) then
                count_x <= (others => '0');
                pos_x <= (others => '0');
                count_y <= count_y + 1;
                if count_y >= (525-1) then
                    count_y <= (others => '0');
                end if;
            end if;

            if count_y < V_SYNC_PULSE-1 then
                s_vsync <= '1';
            end if;
        end if;
    end process;
end Behavioral;