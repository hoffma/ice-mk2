-------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_soc_tb is
end ice_soc_tb;

architecture Behavioral of ice_soc_tb is

    signal clk : std_logic := '0';
    signal clk90 : std_logic := '0';
    constant clk_period : time := 40 ns;

    signal reset : std_logic;
    signal led : std_logic_vector(7 downto 0);
        
begin

    reset <= '1', '0' after 5 ns; --, '1' after 400 ns, '0' after 410 ns;
    -- generate clocks 
    clk_process :process
    begin
        clk <= '0';
        clk90 <= '0';
        wait for clk_period/4;  
        clk <= '1';
        clk90 <= '0';
        wait for clk_period/4;  
        clk <= '1';
        clk90 <= '1';
        wait for clk_period/4;  
        clk <= '0';
        clk90 <= '1';
        wait for clk_period/4;  
    end process;

    dut : entity work.ice_soc port map(clk => clk,
                                        clk90 => clk90,
                                        reset => reset,
                                        led => led);

end Behavioral;
