module ice_pc(
    input clk,
    input reset,
    input load,
    input [11:0] jmp_addr,
    output reg [11:0] pc
);

wire i_clk;
assign i_clk = clk;

always @(posedge i_clk)
begin
    pc <= pc + 1;

    if(load == 1) begin
        pc <= jmp_addr;
    end
    if(reset == 1) begin
        pc <= 0;
    end

end

endmodule
