----------------------------------------------------------------------------------
-- Author: Mario Hoffmann
-- Date: 31.07.2020
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ice_alu_tb is
end ice_alu_tb;

architecture Behavioral of ice_alu_tb is

	signal s_a : STD_LOGIC_VECTOR (15 downto 0);
	signal s_b : STD_LOGIC_VECTOR (15 downto 0);
	signal s_y : STD_LOGIC_VECTOR (15 downto 0);
	signal s_opc : STD_LOGIC_VECTOR (4 downto 0);
    signal s_c : STD_LOGIC;
	signal s_imm : STD_LOGIC_VECTOR(7 downto 0);
	
begin

    dut : entity work.ice_alu port map ( a => s_a,
                                    b => s_b,
                                    imm => s_imm,
									y => s_y,
									opc => s_opc,
									c => s_c);


    proc_stim : process
    begin
        s_a <= x"000a"; -- , x"0007" after 550 ns;
        s_b <= x"0003"; -- , x"000a" after 550 ns;
        s_imm <= x"02";
        s_opc <= "00110";
        wait for 100 ns;

        s_opc <= "00111";
        wait for 100 ns;

        s_opc <= "01000";
        wait for 100 ns;

        s_opc <= "01001";
        wait for 100 ns;

        s_opc <= "01010";
        wait for 100 ns;

        s_opc <= "01011";
        wait for 100 ns;

        s_opc <= "01100";
        wait for 100 ns;

        s_opc <= "01101";
        wait for 100 ns;

        s_opc <= "01110";
        wait for 100 ns;

        s_opc <= "01111";
        wait for 100 ns;


        s_opc <= "10000";
        wait for 100 ns;

        s_opc <= "10001";
        wait for 100 ns;

        s_opc <= "10010";
        wait for 100 ns;

        -- s_a <= x"000a"; -- , x"0007" after 550 ns;
        s_imm <= x"fe";
        s_opc <= "10000";
        wait for 100 ns;

        s_opc <= "10001";
        wait for 100 ns;

    end process;

end Behavioral;
