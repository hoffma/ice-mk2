ld r0 0
ld r1 1
ld r2 0
ld r3 0

ld r5 10


.label %loop
ldm r3 r2
stm r5 r3
jal %delayloop
ld r2 1
ldm r3 r2
stm r5 r3
jal %delayloop
ld r2 2
ldm r3 r2
stm r5 r3
jal %delayloop
ld r2 3
ldm r3 r2
stm r5 r3
jal %delayloop
ld r2 4
ldm r3 r2
stm r5 r3
jal %delayloop
ld r2 5
ldm r3 r2
stm r5 r3
jal %delayloop
jal 0




.label %delayloop
    ld r3 0
    ld r4 1
    ld r10 30
    .label %innerloop
    add r3 r3 r4
    slt r6 r3 r10
    bne r6 %innerloop
    jr r15
